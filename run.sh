#!/bin/bash
echo "Running Famro...  you will need internet connection."
export FAMRO_TCP_MODE=${FAMRO_TCP_MODE:=1}
export FAMRO_HTTP_PORT=${FAMRO_HTTP_PORT:="8000"}
export FAMRO_DATABASE_BACKUP_FOLDER=${FAMRO_DATABASE_BACKUP_FOLDER:="/tmp/db_backups"}
export FAMRO_DATABASE_NAME=${FAMRO_DATABASE_NAME:="postgres"}
export FAMRO_DATABASE_USER=${FAMRO_DATABASE_USER:="postgres"}
export FAMRO_DATABASE_PASSWORD=${FAMRO_DATABASE_PASSWORD:="postgres"}
export FAMRO_SUPERUSER=${FAMRO_SUPERUSER:="admin"}
export FAMRO_SUPERUSER_EMAIL=${FAMRO_SUPERUSER_EMAIL:="admin@testing.here"}
export FAMRO_SUPERUSER_PASSWORD=${FAMRO_SUPERUSER_PASSWORD:="password"}
export FAMRO_DATABASE_HOST=${FAMRO_DATABASE_HOST:="127.0.0.1"}
export FAMRO_DATABASE_PORT=${FAMRO_DATABASE_PORT:="5432"}
export FAMRO_TESTMODE=${FAMRO_TESTMODE:="1"}
export FAMRO_DEBUG=${FAMRO_DEBUG:="1"}

# postgres
docker stop famro-postgres
docker run -d --rm=true --name famro-postgres -p 5432:5432 -e POSTGRES_DB=$FAMRO_DATABASE_NAME -e POSTGRES_USER=$FAMRO_DATABASE_USER -e POSTGRES_PASSWORD=$FAMRO_DATABASE_PASSWORD postgres
docker start famro-postgres

mkdir /tmp/db_backups
rm -rf ./famtree/migrations/*
rm -rf ./venv

virtualenv --clear ./venv -p python3.6
. ./venv/bin/activate

pip install -r ./requirements.txt

RC=1
while [ $RC -eq 1 ]
do
  echo 'Testing database connection...'
  python check_db.py
  RC=$?
done

# Perform the initial schema migrations
python manage.py makemigrations  --noinput

# generate migrations for apps
python manage.py makemigrations famtree --noinput

# generate empty migrations for apps
#python manage.py makemigrations --empty famtree --noinput

# applying migrations
python manage.py migrate

# create new apps
# python manage.py startapp <app_name>

python manage.py shell -c "from django.contrib.auth.models import User; User.objects.create_superuser('$FAMRO_SUPERUSER', '$FAMRO_SUPERUSER_EMAIL', '$FAMRO_SUPERUSER_PASSWORD')"
#python manage.py createsuperuser --username=$FAMRO_SUPERUSER --password=$FAMRO_SUPERUSER_PASSWORD

python init_db.py

if [ $FAMRO_TESTMODE -eq "1" ]; then
    python init_db_test.py
fi

# Start the server
echo "Visit /graphql/ or /persons/"

python manage.py collectstatic --settings=famro.settings --noinput

if [ $FAMRO_TCP_MODE -eq "1" ]; then
  python manage.py runserver 0.0.0.0:$FAMRO_HTTP_PORT
  exit
fi

uwsgi --ini uwsgi.ini
while sleep 3600; do :; done
