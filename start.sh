#!/bin/bash

pg_ctl start

export FAMRO_TCP_MODE=${FAMRO_TCP_MODE:=0}
export FAMRO_HTTP_PORT=${FAMRO_HTTP_PORT:="8000"}
export FAMRO_DATABASE_NAME=${FAMRO_DATABASE_NAME:="famro"}
export FAMRO_DATABASE_USER=${FAMRO_DATABASE_USER:="famro"}
export FAMRO_DATABASE_PASSWORD=${FAMRO_DATABASE_PASSWORD:="famro"}
export FAMRO_DATABASE_HOST=${FAMRO_DATABASE_HOST:="127.0.0.1"}
export FAMRO_DATABASE_PORT=${FAMRO_DATABASE_PORT:="5432"}
export FAMRO_SUPERUSER=${FAMRO_SUPERUSER:="admin"}
export FAMRO_SUPERUSER_EMAIL=${FAMRO_SUPERUSER_EMAIL:="admin@testing.here"}
export FAMRO_SUPERUSER_PASSWORD=${FAMRO_SUPERUSER_PASSWORD:="password"}
export FAMRO_TESTMODE=${FAMRO_TESTMODE:="0"}

psql -U postgres -c "CREATE USER $FAMRO_DATABASE_USER WITH SUPERUSER PASSWORD '$FAMRO_DATABASE_PASSWORD';"
psql --command "CREATE DATABASE $FAMRO_DATABASE_NAME;"

if [ $FAMRO_TESTMODE -eq "1" ]; then
    export FAMRO_DATABASE_NAME=famro_test
fi

RC=1
while [ $RC -eq 1 ]
do
  echo 'Testing database connection...'
  python check_db.py
  RC=$?
done

# Perform the initial schema migrations
python manage.py makemigrations  --noinput

# generate migrations for apps
python manage.py makemigrations famtree --noinput

# generate empty migrations for apps
#python manage.py makemigrations --empty famtree --noinput

# applying migrations
python manage.py migrate

# create new apps
# python manage.py startapp <app_name>

python manage.py shell -c "from django.contrib.auth.models import User; User.objects.create_superuser('$FAMRO_SUPERUSER', '$FAMRO_SUPERUSER_EMAIL', '$FAMRO_SUPERUSER_PASSWORD')"
#python manage.py createsuperuser --username=$FAMRO_SUPERUSER --password=$FAMRO_SUPERUSER_PASSWORD

if [ $FAMRO_TESTMODE -eq "1" ]; then
    python manage.py test -v 2
    python init_db.py
fi

# Start the server
echo "Visit /graphql/"

python manage.py collectstatic --settings=famro.settings --noinput

if [ $FAMRO_TCP_MODE -eq "1" ]; then
  python manage.py runserver 0.0.0.0:$FAMRO_HTTP_PORT
  exit
fi

uwsgi --ini uwsgi.ini
while sleep 3600; do :; done
