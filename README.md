This is the family tree of our family.

To run the app:
`docker run -it --name famro-postgres -v famro-data:/var/lib/postgresql/data -p 8000:8000 -e FAMRO_TESTMODE=1 dionyself/famro:latest`

To see the the volume mountpoint (for restoring db_backups):
`docker volume inspect famro-data`

To start de container with all data:
docker start -a famro-postgres

To connect a shell:
docker exec -it famro-postgres /bin/bash

To stop:
docker stop famro-postgres

you will be running the app locally, visit http://127.0.0.1:8000/graphql/ and make the following query:

```
{Persons(first:500) {
  edges {
    node {
      name
      mother {
        name
      }
      father {
        name
      }
      birthDate
      deathDate
      relationships(first:500) {
        edges {
          node {
            name
          }
        }
      }
      children(first:500) {
        edges {
          node {
            name
          }
        }
      }
    }
  }
}}
```

It will show all the persons registered in the tree

build:
`docker build --force-rm -t dionyself/famro .``
push:
`docker push dionyself/famro:latest`


Doc Indexation guide:
1) Index Places and Residences
2) Index The source
3) Index/update all Persons, but the main persons shouldbe worked first
4) If it is a marriage, index the relationship too
5) review the saved info
6) create source tags
