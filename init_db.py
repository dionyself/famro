#!/usr/bin/env python
import os
from datetime import date
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "famro.settings")
django.setup()

from django.conf import settings
from famtree.models import PDFTemplate

try:
    _, _ = PDFTemplate.objects.update_or_create(
        source_type="baptism_male", defaults={
            "space_mapper": {
                "source_type_name": 15,
                "source_container_number": 15,
                "source_page_number": 3,
                "source_place": 15,
                "source_house": 10,
                "source_date": 8,
                "main_male_name": 10,
                "main_male_born_on": 10,
                "main_male_full_name": 20,
            },
            "mapper": {
                "source_type_name": 0,
                "source_container_number": 0,
                "source_page_number": 0,
                "source_place": 0,
                "source_house": [0, 1],
                "source_date": 1,
                "main_male": {
                    "name": 1,
                    "born_on": 1,
                    "full_name": 0,
                    }
                },
            "body": [
                { 'name': 'content_header', 'multiline': True, 'type': 'T', 'x1': 20.0, 'y1': 100.5, 'x2': 170.0, 'y2': 104.5, 'font': 'Arial', 'size': 12.0, 'bold': 1, 'italic': 0, 'underline': 0, 'foreground': 0, 'background': 0, 'align': 'L', 'priority': 1,
                    'text': """La Sociedad Genealógica Del Rosario Certifica haber examinado e indexado un registro correspondiente a {source_type_name} de {main_male_full_name},
     ubicado en el numero de registro {registry_number}, Folio {source_page_number} del libro {source_container_number} de {source_container_name} de {source_house} de {source_place} y cuyo contenido certificamos se lee de la siguiente manera:"""},
                { 'name': 'content_body', 'multiline': True, 'type': 'T', 'x1': 20.0, 'y1': 100.5, 'x2': 170.0, 'y2': 104.5, 'font': 'Arial', 'size': 12.0, 'bold': 1, 'italic': 0, 'underline': 0, 'foreground': 0, 'background': 0, 'align': 'L', 'priority': 1,
                    'text': 'En la {source_house} de {source_place} el dia {source_date} yo el infrascrito cura interino de ella, bautice solemnemente, puse oleo y crisma a {main_male_name}, que nacio en {main_male_birth_place}, el dia {main_male_born_on} hijo natural de {mother_name}, fueron sus padrinos, {godfather} y {godmother} a quienes advertí el parentezco espiritual y obligaciones, doy fe.  (firmado por{authority_name})',},
                { 'name': 'content_footer', 'multiline': True, 'type': 'T', 'x1': 20.0, 'y1': 100.5, 'x2': 170.0, 'y2': 104.5, 'font': 'Arial', 'size': 12.0, 'bold': 1, 'italic': 0, 'underline': 0, 'foreground': 0, 'background': 0, 'align': 'L', 'priority': 1,
                    'text': "Nota: -----------Fin de la indexacion------No mas info debajo de esta linea-----------"},
                { 'name': 'line1', 'type': 'L', 'x1': 45.0, 'y1': 220.0, 'x2': 130.0, 'y2': 220.0, 'font': 'Arial', 'size': 0, 'bold': 0, 'italic': 0, 'underline': 0, 'foreground': 0, 'background': 0, 'align': 'I', 'text': None, 'priority': 3, },
                { 'name': 'barcode', 'type': 'BC', 'x1': 35.0, 'y1': 246.5, 'x2': 140.0, 'y2': 254.0, 'font': 'Interleaved 2of5 NT', 'size': 0.75, 'bold': 0, 'italic': 0, 'underline': 0, 'foreground': 0, 'background': 0, 'align': 'I', 'text': '200000000001000159053338016581200810081', 'priority': 3, },
                # { 'name': 'main_box', 'type': 'B', 'x1': 15.0, 'y1': 15.0, 'x2': 185.0, 'y2': 260.0, 'font': 'Arial', 'size': 0.0, 'bold': 0, 'italic': 0, 'underline': 0, 'foreground': 0, 'background': 0, 'align': 'L', 'text': None, 'priority': 0, },
                { 'name': 'emiter_entity', 'type': 'T', 'x1': 57.0, 'y1': 22.5, 'x2': 115.0, 'y2': 30.5, 'font': 'Arial', 'size': 12.0, 'bold': 1, 'italic': 0, 'underline': 0, 'foreground': 0, 'background': 0, 'align': 'I', 'text': 'Sociedad Genealógica Del Rosario', 'priority': 2, },
                { 'name': 'document_logo', 'type': 'I', 'x1': 57.0, 'y1': 30.0, 'x2': 105.0, 'y2': 80.0, 'font': None, 'size': 0.0, 'bold': 0, 'italic': 0, 'underline': 0, 'foreground': 0, 'background': 0, 'align': 'I', 'text': './famtree/static/images/escudo_dominicano.png', 'priority': 2, },
                { 'name': 'document_type', 'type': 'T', 'x1': 57.0, 'y1': 90.5, 'x2': 115.0, 'y2': 100.5, 'font': 'Arial', 'size': 12.0, 'bold': 1, 'italic': 0, 'underline': 0, 'foreground': 0, 'background': 0, 'align': 'I', 'text': 'Certificado De Indexación', 'priority': 2, },
            ]}
        )
except Exception as e:
    print(e, "##########################################################################")
