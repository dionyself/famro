import subprocess
import sys, os
from crequest.middleware import CrequestMiddleware
from django.contrib.sites.shortcuts import get_current_site
from django.core.files.storage import FileSystemStorage 
from rest_framework import serializers
from django.db.models.manager import Manager
from django.db.models.query import QuerySet

from famtree.models import Person, Place, Source, Tag, Relationship, make_person, Residence, Emigration
from .hacks import DynamicField, DatabaseBackup


def get_my_person_limited_queryset(self):
    get_my_person_limited_queryset.get = Person.objects.get
    root = self.root
    related_persons = []
    related_persons.extend(Person.objects.all().order_by('-id')[:50])
    current_request = CrequestMiddleware.get_request()
    related_person_id = int(current_request.GET.get('related_person_id', "0"))
    if related_person_id:
        related_persons.extend(Person.get_related_persons_by_id(person_id=related_person_id))
    if root.instance:
        if isinstance(root.instance, Relationship):
            if root.instance.groom:
                related_persons.extend(root.instance.groom.get_filtered_related_persons())
            if root.instance.bride:
                related_persons.extend(root.instance.bride.get_filtered_related_persons())
        if isinstance(root.instance, Person):
            related_persons.extend(root.instance.get_filtered_related_persons())
    return related_persons
    
def get_my_places_limited_queryset(self):
    get_my_places_limited_queryset.get = Place.objects.get
    related_places = list(Place.objects.all().order_by('-id')[:50])
    related_persons = get_my_person_limited_queryset(self)
    for person in related_persons:
        if person.birth_place:
            related_places.append(person.birth_place)
        if person.death_place:
            related_places.append(person.death_place)
        if person.baptism_place:
            related_places.append(person.baptism_place)
        if person.paternity_acknowledgment_place:
            related_places.append(person.paternity_acknowledgment_place)
    root = self.root
    if root.instance:
        if isinstance(root.instance, Person):
            search_place = root.instance.birth_place or root.instance.death_place
            if search_place:
                related_places.extend(Place.objects.filter(country=search_place.country).all()[:120])
        if isinstance(root.instance, Place):
            related_places.extend(Place.objects.filter(country=root.instance.country).all()[:120])
    return related_places
    
def get_my_sources_limited_queryset(self):
    get_my_sources_limited_queryset.get = Source.objects.get
    related_sources = list(Source.objects.all().order_by('-id')[:50])
    
    related_places = get_my_places_limited_queryset(self)
    for place in related_places:
        related_sources.extend(place.sources.all()[:120])
    root = self.root
    if root.instance:
        if isinstance(root.instance, Source):
            related_sources.extend(
                Source.objects.filter(
                    source_place__country=root.instance.country).all()[:50])
            for person in root.instance.persons.all():
                related_sources.extend(person.sources.all())
        if isinstance(root.instance, Person):
            for source in root.instance.sources.all():
                for person in source.persons.all():
                    related_sources.extend(person.sources.all())
    return related_sources


def get_python_executable():
    try:
        import uwsgi
        print(
            uwsgi.opt,
            uwsgi.opt['virtualenv'],
            uwsgi.opt['home'],
            "#####################################"
        )
        return
    except:
        import sys
        return sys.executable
    return "python"


class DatabaseBackupSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False)
    host = serializers.CharField(required=False)
    db_name = serializers.CharField(required=False)
    username = serializers.CharField(required=False)
    password = serializers.CharField(required=False, write_only=True)
    filename = serializers.CharField(required=False)
    upload_backup_file = serializers.FileField(required=False, write_only=True)
    download_url = serializers.CharField(required=False, read_only=True)
    src_images_download_url = serializers.SerializerMethodField(required=False, read_only=True)
    delete = serializers.BooleanField(required=False, write_only=True)
    
    def get_src_images_download_url(self, obj):
        current_request = CrequestMiddleware.get_request()
        base_url = 'http://'+get_current_site(current_request).domain
        return "wget -N -l 20 -rp --no-parent --reject 'index.html*' -e robots=off " + f"{base_url}/media/images/"

    @staticmethod
    def get_backup_list(pk=None, base_url=""):
        get_python_executable()
        result = subprocess.check_output([sys.executable, "manage.py", "listbackups"])
        lines = result.decode().split("\n")
        headers = lines.pop(0).split()
        results = []
        for index, line in enumerate(lines):
            if line:
                results.append(DatabaseBackup(id=index, filename=line.split()[0], download_url=base_url+"/backup-download/"+line.split()[0]))
        if pk != None:
            return results[pk]
        results.reverse()
        return results

    def create(self, validated_data):
        id = validated_data.pop("id", None)
        if id is None:
            file = validated_data.get("upload_backup_file")
            if file:
                fs = FileSystemStorage(location=os.environ.get("FAMRO_DATABASE_BACKUP_FOLDER", "/var/lib/postgresql/data/db_backups"))
                validated_data["filename"] = fs.save(file.name, file)
                return DatabaseBackup(id=id, **validated_data)
            args = [sys.executable, "manage.py", "dbbackup"]
            subprocess.run(args, stdout=subprocess.PIPE) 
        elif (id is not None)  and validated_data.pop("delete", None):
            to_delete = self.get_backup_list(pk=id)
            args = ["rm", os.environ.get("FAMRO_DATABASE_BACKUP_FOLDER", "/var/lib/postgresql/data/db_backups") + "/" + to_delete.filename]
            subprocess.run(args, stdout=subprocess.PIPE) 
        else:
            to_restore = self.get_backup_list(pk=id)
            args = [sys.executable, "manage.py", "dbrestore", "-i", to_restore.filename, "--noinput", "&&",
                sys.executable, "manage.py", "makemigrations", "--noinput", "&&",
                sys.executable,  "manage.py", "makemigrations", "famtree", "--noinput", "&&",
                sys.executable,  "manage.py", "migrate"]
            args = " ".join(args)
            subprocess.Popen(args, shell=True)
            #args = ["sh", "run_migrations.sh"]
            #subprocess.Popen(args, shell=True)
        return DatabaseBackup(id=id, **validated_data)


class EmigrationSerializer(serializers.HyperlinkedModelSerializer):
    person = DynamicField(view_name='person-detail', allow_null=True, required=False, queryset=get_my_person_limited_queryset)
    _from = DynamicField(view_name='place-detail', required=False, queryset=get_my_places_limited_queryset)
    to = DynamicField(view_name='place-detail', required=False, queryset=get_my_places_limited_queryset)
    owner = serializers.PrimaryKeyRelatedField(required=False, read_only=True)
    created_at = serializers.DateTimeField(read_only=True)
    updated_at = serializers.DateTimeField(read_only=True)

    class Meta:
        model = Emigration
        fields = (
            "id",
            "person",
            "_from",
            "to",
            "date",
            "properties",
            "comments",
            "owner",
            "is_deleted",
            "created_at",
            "updated_at",
            )


class RelationshipSerializer(serializers.HyperlinkedModelSerializer):
    groom = DynamicField(view_name='person-detail', allow_null=True, required=True, queryset=get_my_person_limited_queryset)
    bride = DynamicField(view_name='person-detail', allow_null=True, required=True, queryset=get_my_person_limited_queryset)
    children = DynamicField(view_name='person-detail', many=True, required=False, queryset=get_my_person_limited_queryset)
    marriage_authorizers = DynamicField(view_name='person-detail', many=True, required=False, queryset=get_my_person_limited_queryset)
    marriage_godfather = DynamicField(view_name='person-detail', allow_null=True, required=False, queryset=get_my_person_limited_queryset)
    marriage_godmother = DynamicField(view_name='person-detail', allow_null=True, required=False, queryset=get_my_person_limited_queryset)
    marriage_witnesses = DynamicField(view_name='person-detail', many=True, required=False, queryset=get_my_person_limited_queryset)
    marriage_places = DynamicField(view_name='place-detail', many=True, required=False, queryset=get_my_places_limited_queryset)
    #marriage_sources = DynamicField(view_name='source-detail', many=True, required=False, queryset=get_my_sources_limited_queryset)
    owner = serializers.PrimaryKeyRelatedField(required=False, read_only=True)
    created_at = serializers.DateTimeField(read_only=True)
    updated_at = serializers.DateTimeField(read_only=True)

    class Meta:
        model = Relationship
        fields = (
            "id",
            "groom",
            "bride",
            "children",
            "marriage_witnesses",
            "marriage_godfather",
            "marriage_godmother",
            "marriage_places",
            "marriage_authorizers",
            "properties",
            "start_date",
            "end_date",
            "comments",
            "is_deleted",
            "owner",
            "created_at",
            "updated_at",
            )


class TagSerializer(serializers.HyperlinkedModelSerializer):

    created_at = serializers.DateTimeField(read_only=True)
    updated_at = serializers.DateTimeField(read_only=True)

    class Meta:
        model = Tag
        fields = (
            "id",
            "kindred_type",
            "registry_number",
            "source",
            "place_area",
            "residence_area",
            "tag_date_area",
            "person",
            "residence",
            "place",
            "person_age_area",
            "person_age",
            "person_area",
            "properties",
            "comments",
            "is_deleted",
            "created_at",
            "updated_at",
            )


class SourceSerializer(serializers.HyperlinkedModelSerializer):

    image = serializers.FileField(required=False)
    qr_image_url = serializers.SerializerMethodField(required=False, read_only=True) #serializers.MethodField(required=False, read_only=True)
    persons = DynamicField(view_name='person-detail', many=True, required=False, queryset=get_my_person_limited_queryset)
    #source_place = serializers.PrimaryKeyRelatedField(required=False, queryset=Place.objects)
    source_place = DynamicField(view_name='place-detail', allow_null=True, required=False, queryset=get_my_places_limited_queryset)
    created_at = serializers.DateTimeField(read_only=True)
    updated_at = serializers.DateTimeField(read_only=True)
    def get_qr_image_url(self, obj):
        return obj.qr_image_url
    class Meta:
        model = Source
        fields = (
            "id",
            "name",
            "url",
            "image",
            "qr_image_url",
            "source_type_name",
            "source_type",
            "source_date",
            "source_place",
            "persons",
            "properties",
            "comments",
            "is_deleted",
            "created_at",
            "updated_at",
            )


class PlaceSerializer(serializers.HyperlinkedModelSerializer):

    created_at = serializers.DateTimeField(read_only=True)
    updated_at = serializers.DateTimeField(read_only=True)

    class Meta:
        model = Place
        fields = (
            "id",
            "country",
            "state",
            "city",
            "neighborhood",
            "house",
            "street",
            "place_type",
            "properties",
            "comments",
            "is_deleted",
            "created_at",
            "updated_at",
            )

class ResidenceSerializer(serializers.HyperlinkedModelSerializer):

    residents = serializers.PrimaryKeyRelatedField(many=True, required=False, read_only=True)
    residence_place = DynamicField(view_name='place-detail', allow_null=True, required=False, queryset=get_my_places_limited_queryset)
    created_at = serializers.DateTimeField(read_only=True)
    updated_at = serializers.DateTimeField(read_only=True)

    class Meta:
        model = Residence
        fields = (
            "id",
            "residents",
            "residence_place",
            "comments",
            "properties",
            "is_deleted",
            "created_at",
            "updated_at",
            )

class PersonSerializer(serializers.HyperlinkedModelSerializer):

    father = DynamicField(view_name='person-detail', allow_null=True, required=False, queryset=get_my_person_limited_queryset)
    mother = DynamicField(view_name='person-detail', allow_null=True, required=False, queryset=get_my_person_limited_queryset)
    birth_witnesses = DynamicField(view_name='person-detail', many=True, required=False, queryset=get_my_person_limited_queryset)
    birth_godmother = DynamicField(view_name='person-detail', allow_null=True, required=False, queryset=get_my_person_limited_queryset)
    birth_godfather = DynamicField(view_name='person-detail', allow_null=True, required=False, queryset=get_my_person_limited_queryset)
    death_witnesses  = DynamicField(view_name='person-detail', many=True, required=False, queryset=get_my_person_limited_queryset)
    baptism_godmother = DynamicField(view_name='person-detail', allow_null=True, required=False, queryset=get_my_person_limited_queryset)
    baptism_godfather = DynamicField(view_name='person-detail', allow_null=True, required=False, queryset=get_my_person_limited_queryset)
    baptism_witnesses = DynamicField(view_name='person-detail', many=True, required=False, queryset=get_my_person_limited_queryset)
    paternity_acknowledgment_witnesses = DynamicField(view_name='person-detail', many=True, required=False, queryset=get_my_person_limited_queryset)
    parents_marriage = serializers.PrimaryKeyRelatedField(allow_null=True, write_only=True, required=False, queryset=Relationship.objects)
    fingerprint = serializers.CharField(read_only=True)
    husband_surname = serializers.CharField(read_only=True)
    sources = DynamicField(view_name='source-detail', many=True, required=False, queryset=get_my_sources_limited_queryset)
    #death_sources = DynamicField(view_name='source-detail', many=True, required=False, queryset=get_my_sources_limited_queryset)
    birth_is_legitim = serializers.BooleanField(default=True)
    is_surname_freezed = serializers.BooleanField(default=True)
    owner = serializers.PrimaryKeyRelatedField(required=False, read_only=True)
    created_at = serializers.DateTimeField(read_only=True)
    updated_at = serializers.DateTimeField(read_only=True)
    
    tree_node = serializers.SerializerMethodField(required=False, read_only=True)
    def get_tree_node(self, obj):
        return obj.tree_node
    
    related_persons = serializers.SerializerMethodField(required=False, read_only=True)
    def get_related_persons(self, obj):
        return ({"id": rel_p.pk, "name": str(rel_p)} for rel_p in obj.get_filtered_related_persons())

    def validate(self, kwargs):
        match = kwargs.pop("parents_marriage", None)
        kwargs = make_person(match=match, validate_only=True, **kwargs)
        return super(PersonSerializer, self).validate(kwargs)

    class Meta:
        model = Person
        fields = (
            "id",
            "fingerprint",
            "name",
            "father_surname",
            "mother_surname",
            "birth_date",
            "parents_marriage",
            "mother",
            "father",
            "birth_is_legitim",

            "birth_declared_by",
            "birth_place",
            "birth_witnesses",
            "birth_authorized_by",
            "birth_properties",
            "birth_godmother",
            "birth_godfather",

            "baptism_authorized_by",
            "baptism_date",
            "baptism_place",
            "baptism_witnesses",
            "baptism_properties",
            "baptism_godmother",
            "baptism_godfather",
    
            "paternity_acknowledgment_date",
            "paternity_acknowledgment_place",
            "paternity_acknowledgment_properties",
            "paternity_acknowledgment_witnesses",
            "paternity_acknowledgment_authorized_by",

            "career",
            "husband_surname",
            
            "death_authorized_by",
            "death_declared_by",
            "death_witnesses",
            "death_place",
            "death_date",
            "death_properties",
            "properties",
            "comments",
            "is_deleted",
            "created_at",
            "updated_at",
            "is_surname_freezed",
            "related_persons",
            "sources",
            "tree_node",
            "owner"
            )
