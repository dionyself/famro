
from __future__ import unicode_literals
import json
import datetime
from django.db import models
from django.contrib.postgres.fields import JSONField
from pytz import timezone
from django.forms.models import model_to_dict
from crequest.middleware import CrequestMiddleware
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth.models import User


# Create your models here.

class Place(models.Model):
    country = models.TextField(null=True, blank=True)
    state = models.TextField(null=True, blank=True)
    city = models.TextField(null=True, blank=True)
    neighborhood = models.TextField(null=True, blank=True)
    house = models.TextField(null=True, blank=True, help_text="Name of the church or office")
    street = models.TextField(null=True, blank=True)
    place_type = models.TextField(null=True, blank=True)
    properties = JSONField(null=True, blank=True, help_text="")
    comments = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_deleted = models.BooleanField(default=False, help_text="True if deleted.")

    def save(self, *args, **kwargs):
        updated = state = country = ""
        if self.pk:
            old_place = self.__class__.objects.get(pk=self.pk)
            country = str(old_place.country)
            state = str(old_place.state)
            updated = True
            del old_place
        super(Place, self).save(*args, **kwargs)
        self.refresh_from_db()
        if updated and (country != str(self.country) or state != str(self.state)):
            for birth in self.births.all():
                birth.update_person()

    def __str__ (self):
        return f"Place({self.country}, {self.state}, {self.city}, {self.neighborhood}, {self.house}, {self.place_type})"

    class Meta:
        ordering = ["-id"]

def get_source_path(instance, filename):
    place = {}
    place_data = {}
    try:
        if instance.source_place_id:
            place = model_to_dict(Place.objects.get(pk=instance.source_place_id))
        else:
            place = model_to_dict(Place.objects.get(pk=instance.source_place.id))
    except Exception as e:
        print(e)
    
    for key in ["country", "state", "city", "neighborhood", "place_type", "house"]:
        place_data[key] = place.get(key) or "no_{0}".format(key)
    
    path = "images/{country}/{state}/{city}/{neighborhood}/{place_type}/{house}/".format(**place_data)
    return path + filename


class PDFTemplate(models.Model):
    source_type = models.TextField(unique=True, null=True, blank=True)
    title = models.TextField(null=True, blank=True)
    mapper = JSONField(null=True, blank=True, help_text="")
    space_mapper = JSONField(null=True, blank=True, help_text="")
    body = JSONField(null=True, blank=True, help_text="")
    properties = JSONField(null=True, blank=True, help_text="")
    name = models.TextField(unique=True, null=True, blank=True)
    comments = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_deleted = models.BooleanField(default=False, help_text="True if deleted.")


class Source(models.Model):
    name = models.TextField(null=True, blank=True)
    url = models.TextField(null=True, blank=True)
    # valid source_types:
    # canonical_mariage_legitim_legitim, canonical_mariage_legitim_natural,
    # canonical_mariage_natural_natural, canonical_mariage_natural_legitim
    # civil_mariage_legitim_legitim, civil_mariage_legitim_natural,
    # civil_mariage_natural_natural, civil_mariage_natural_legitim
    # baptism_male_legitim, baptism_male, baptism_female_legitim, baptism_female
    # born_male_legitim, born_male, born_female_legitim, born_female
    # canonical_death_male_legitim, canonical_death_male, canonical_death_female_legitim, canonical_death_female
    # civil_death_male_legitim, civil_death_male, civil_death_female_legitim, civil_death_female
    # civil_death_male_legitim, civil_death_male, civil_death_female_legitim, civil_death_female
    # civil_death_male_legitim, civil_death_male, civil_death_female_legitim, civil_death_female
    # acknowledge_male, acknowledge_female
    source_type = models.TextField(null=True, blank=True)
    source_type_name = models.TextField(null=True, blank=True)
    properties = JSONField(null=True, blank=True, help_text="")
    source_date = models.DateField(null=True, blank=True)
    source_place = models.ForeignKey(Place, default=None, null=True, blank=True, related_name="sources", on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_deleted = models.BooleanField(default=False, help_text="True if deleted.")
    comments = models.TextField(null=True, blank=True)
    image = models.FileField(upload_to=get_source_path, null=True, blank=True, max_length=500)
    
    def save(self, *args, **kwargs):
        if self.url and not self.image:
            old_source = self.__class__.objects.filter(url=self.url, image__isnull=False).first()
            if old_source:
                self.image = old_source.image
        return super(Source, self).save(*args, **kwargs)
    
    @property
    def qr_image_data(self):
        return f"{self.name or ''}\n{self.url or ''}\n{self.image.url or ''}\nmailto:dionyself@gmail.com"
    
    @property
    def qr_image_url(self):
        current_request = CrequestMiddleware.get_request()
        base_url = 'http://'+get_current_site(current_request).domain
        return f"{base_url}/source_qr_image/{self.id}"
    
    @property
    def printable_source_url(self):
        current_request = CrequestMiddleware.get_request()
        base_url = 'http://'+get_current_site(current_request).domain
        return f"{base_url}/source_printable_image/{self.id}"

    def __str__ (self):
        return f"Source({self.name or ''}, {self.source_place and self.source_place.state or ''}, {self.source_date or ''})"

    class Meta:
        ordering = ["-id"]

class Person(models.Model):
    fingerprint = models.TextField(unique=True, null=True, blank=True, help_text="Fingerprint.")
    career = models.TextField(null=True, blank=True, help_text="Optional, carrer")
    mother = models.ForeignKey("self", default=None, null=True, blank=True, related_name="mother_of", on_delete=models.CASCADE, help_text="Mother")
    father = models.ForeignKey("self", default=None, null=True, blank=True, related_name="father_of", on_delete=models.CASCADE, help_text="Possible Father, considered confirmed if legitimated on acknowledge")
    husband_surname = models.TextField(null=True, blank=True, help_text="Husband`s surname")
    sources = models.ManyToManyField(Source, related_name="persons")

    #paternity_acknowledgment_source = models.ForeignKey(Source, default=None, null=True, blank=True, related_name="acknowledged_children", on_delete=models.CASCADE)
    paternity_acknowledgment_date = models.DateField(null=True)
    paternity_acknowledgment_place = models.ForeignKey(Place, default=None, null=True, blank=True, related_name="paternity_acknowledgments", on_delete=models.CASCADE)
    paternity_acknowledgment_properties = JSONField(null=True, blank=True, help_text="Serial numbers, page ...")
    paternity_acknowledgment_witnesses = models.ManyToManyField("self", related_name="paternity_acknowledgment_witness_of", help_text="witnesses")
    paternity_acknowledgment_authorized_by = models.ForeignKey("self", default=None, null=True, blank=True, related_name="paternity_acknowledgment_authority_of", on_delete=models.CASCADE, help_text="Civil authority")
    acknowledged_on_marriage = models.ForeignKey("Relationship", default=None, null=True, blank=True, related_name="children", on_delete=models.CASCADE)
    
    #appears_on_source = models.ManyToManyField(Source, related_name="persons")

    name = models.TextField(null=True, blank=True, help_text="Required Name")
    father_surname = models.TextField(null=True, blank=True, help_text="Father`s surname")
    mother_surname = models.TextField(null=True, blank=True, help_text="Mother`s surname")
    birth_is_legitim = models.BooleanField(default=False, help_text="True is birth under legal marriage.")
    birth_date = models.DateField(null=True)
    birth_declared_by = models.ForeignKey("self", default=None, null=True, blank=True, related_name="birth_declarations", on_delete=models.CASCADE)
    birth_place = models.ForeignKey(Place, default=None, null=True, blank=True, related_name="births", on_delete=models.CASCADE)
    birth_witnesses = models.ManyToManyField("self", related_name="birth_witness_of")
    birth_authorized_by = models.ForeignKey("self", default=None, null=True, blank=True, related_name="birth_authority_of", on_delete=models.CASCADE)
    birth_properties = JSONField(null=True, blank=True, help_text="")
    #birth_source = models.ForeignKey(Source, default=None, null=True, blank=True, related_name="births", on_delete=models.CASCADE)
    birth_godmother = models.ForeignKey("self", default=None, null=True, blank=True, related_name="godmother_on_birth_of", on_delete=models.CASCADE, help_text="GodMother")
    birth_godfather = models.ForeignKey("self", default=None, null=True, blank=True, related_name="godfather_on_birth_of", on_delete=models.CASCADE, help_text="GodFather")

    baptism_authorized_by = models.ForeignKey("self", default=None, null=True, blank=True, related_name="baptism_authority_of", on_delete=models.CASCADE)
    baptism_date = models.DateField(null=True)
    baptism_place = models.ForeignKey(Place, default=None, null=True, blank=True, related_name="baptisms", on_delete=models.CASCADE)
    baptism_witnesses = models.ManyToManyField("self", related_name="baptism_witness_of")
    baptism_properties = JSONField(null=True, blank=True, help_text="")
    #baptism_source = models.ForeignKey(Source, default=None, null=True, blank=True, related_name="baptisms", on_delete=models.CASCADE)
    baptism_godmother = models.ForeignKey("self", blank=True, default=None, null=True, related_name="godmother_on_baptism_of", on_delete=models.CASCADE, help_text="baptism GodMother")
    baptism_godfather = models.ForeignKey("self", blank=True, default=None, null=True, related_name="godfather_on_baptism_of", on_delete=models.CASCADE, help_text="baptism GodFather")

    death_authorized_by = models.ForeignKey("self", default=None, null=True, blank=True, related_name="death_authority_of", on_delete=models.CASCADE)
    death_declared_by = models.ForeignKey("self", default=None, null=True, blank=True, related_name="death_declarations", on_delete=models.CASCADE)
    death_witnesses = models.ManyToManyField("self", related_name="death_witness_of")
    death_place = models.ForeignKey(Place, default=None, null=True, blank=True, related_name="deaths", on_delete=models.CASCADE)
    death_date = models.DateField(null=True)
    death_properties = JSONField(null=True, blank=True, help_text="")
    #death_sources = models.ManyToManyField(Source, related_name="deaths")
    #canonical_death_source = models.ForeignKey(Source, null=True, related_name="canonical_deaths", on_delete=models.CASCADE)

    #relationships = models.ManyToManyField("self", blank=True, through='Relationship', through_fields=('groom', 'bride'), symmetrical=False)

    properties = JSONField(null=True, blank=True, help_text="")
    comments = models.TextField(null=True, blank=True)
    is_surname_freezed = models.BooleanField(default=True, help_text="")
    is_deleted = models.BooleanField(default=False, help_text="True if person is deleted.")
    is_hidden = models.BooleanField(default=False, help_text="True if person is hidden.")
    is_blocked = models.BooleanField(default=False, help_text="Prevent others to make changes to this profile")
    owner = models.ForeignKey(User, default=None, null=True, blank=True, related_name="persons", on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__ (self):
        return f"Person(ID-{self.pk}, {self.name} {self.father_surname} {self.mother_surname}, son of {self.father and self.father.name} and {self.mother and self.mother.name}, from {self.birth_place})"

    def get_full_name(self, format="", name_type="", date=None):
        return f"{self.name or ''} {self.father_surname or ''} {self.mother_surname or ''}"
    
    @property
    def children(self):
        return self.mother_of.all() or self.father_of.all()

    @property    
    def tree_url(self):
        current_request = CrequestMiddleware.get_request()
        if current_request and self.id:
            base_url = 'http://'+get_current_site(current_request).domain
            return f"{base_url}/tree/{self.id}/"
        return ""
        
    @property
    def dict_repr(self):
        nationality = self.birth_place.country if self.birth_place else self.death_place.country if self.death_place else ""
        return {
            "id": self.id or 0,
            "full_name": self.get_full_name(),
            "birth_date": str(self.birth_date.year if self.birth_date else "?"),
            "death_date": str(self.death_date.year if self.death_date else "?"),
            "fingerprint": str(self.fingerprint),
            "photo": "",
            "nationality": str(nationality),
            "mother_name": str(self.mother.get_full_name() if self.mother else ""),
            "father_name": str(self.father.get_full_name() if self.father else ""),
            "url": self.tree_url,
            "mother_url": str(self.mother.tree_url if self.mother else self.tree_url),
            "father_url": str(self.father.tree_url if self.father else self.tree_url),
        }
    
    @property    
    def tree_node(self):
        tree = {}
        unknown = {
            "id": self.id or 0,
            "full_name": "Unknown",
            "birth_date": "?",
            "death_date": "?",
            "fingerprint": "",
            "photo": "",
            "nationality": "",
            "mother_name": "",
            "father_name": "",
            "url": "",
            "mother_url": "",
            "father_url": "",
        }
        tree["main"] = self.dict_repr
        tree["father"] = self.father and self.father.dict_repr or unknown
        tree["mother"] = self.mother and self.mother.dict_repr or unknown
        tree["children"] = [child.dict_repr for child in self.children]
        tree["grandchildren"] = [[grandchild.dict_repr for grandchild in child.children] for child in self.children]
        tree["spouses"] = []
        for rel in self.groom_in.all():
            tree["spouses"].append(rel.bride.dict_repr)
        for rel in self.bride_in.all():
            tree["spouses"].append(rel.groom.dict_repr)        
        return tree

    def get_tree_node(self, extended=False, raw=False):
        if raw:
            self.tree_node
        return json.dumps(self.tree_node)

    def get_filtered_related_persons(self, by_surname=False, extended=True, return_queries=False, start=0,limit=50):
        results = []
        persons_queries = [
            self.paternity_acknowledgment_witnesses,
            self.death_witnesses,
            self.baptism_witnesses,
            self.birth_witnesses]
        estimated_relateds = []
        if extended:
            if self.father:
                persons_queries.extend(self.father.get_filtered_related_persons(extended=False, return_queries=True))
                if self.father.father_of:
                    for brother in self.father.father_of.all():
                        persons_queries.extend(brother.get_filtered_related_persons(extended=False, return_queries=True))
                        if brother.father_of:
                            for nefew in brother.father_of.all():
                                persons_queries.extend(nefew.get_filtered_related_persons(extended=False, return_queries=True))
                        if brother.mother_of:
                            for niece in brother.mother_of.all():
                                persons_queries.extend(niece.get_filtered_related_persons(extended=False, return_queries=True))
            if self.mother:
                persons_queries.extend(self.mother.get_filtered_related_persons(extended=False, return_queries=True))
                if self.mother.mother_of:
                    for sister in self.mother.mother_of.all():
                        persons_queries.extend(sister.get_filtered_related_persons(extended=False, return_queries=True))
                        if sister.father_of:
                            for nefew in sister.father_of.all():
                                persons_queries.extend(nefew.get_filtered_related_persons(extended=False, return_queries=True))
                        if sister.mother_of:
                            for niece in sister.mother_of.all():
                                persons_queries.extend(niece.get_filtered_related_persons(extended=False, return_queries=True))
            if self.father_of:
                for son in self.father_of.all():
                    persons_queries.extend(son.get_filtered_related_persons(extended=False, return_queries=True))
            if self.mother_of:
                for dauther in self.mother_of.all():
                    persons_queries.extend(dauther.get_filtered_related_persons(extended=False, return_queries=True))
            if self.get_estimated_life_time():
                life_range = self.get_estimated_life_time(add_margin=20)
                if all(life_range) and any([self.birth_place, self.death_place]):
                    search_place = self.birth_place or self.death_place
                    birth_range = Person.objects.filter(
                        birth_date__range=life_range,
                        birth_place__country=search_place.country,
                        birth_place__state=search_place.state)
                    death_range = Person.objects.filter(
                        death_date__range=life_range,
                        death_place__country=search_place.country,
                        death_place__state=search_place.state)
                    for surname in (self.father_surname, self.mother_surname):
                        if surname:
                            estimated_relateds.append(birth_range.filter(father_surname=surname))
                            estimated_relateds.append(birth_range.filter(mother_surname=surname))
                            estimated_relateds.append(death_range.filter(father_surname=surname))
                            estimated_relateds.append(death_range.filter(mother_surname=surname))    
                
        by_surname_queries = []
        if by_surname:
            for query in persons_queries:
                for surname in (self.father_surname, self.mother_surname):
                    if surname:
                        by_surname_queries.expand([
                            query.filter(father_surname=surname),
                            query.filter(father_surname=surname)
                        ])
                    else:
                        by_surname_queries.append(query)
            persons_queries = by_surname_queries
        
        if return_queries:
            return persons_queries
        for query in persons_queries:
            results.extend(query.all())
        limited_queries = []
        for query in estimated_relateds:
            limited_queries.extend(query.all()[start:limit])
        results.extend(limited_queries)
        return results

    def get_estimated_life_time(self, add_margin=0):
        to_return = [self.birth_date or self.baptism_date, self.death_date]
        if (not to_return[1]) and to_return[0]:
            to_return[1] = datetime.datetime(to_return[0].year + 40, 12, 31, 23, 59)
        if (not to_return[0]) and to_return[1]:
            to_return[0] = datetime.datetime(to_return[1].year - 40, 1,1,0,1)
        if add_margin and all(to_return):
            to_return[0] = datetime.datetime(to_return[0].year - add_margin, 1,1,0,1)
            to_return[1] = datetime.datetime(to_return[1].year + add_margin, 12, 31, 23, 59)
        return to_return

    @property
    def related_persons(self):
        return self.get_filtered_related_persons()
        
    @classmethod
    def get_related_persons_by_id(person_id=0, **kwargs):
        related_persons = []
        if person_id:
            root_person = Person.objects.filter(id=person_id).first()
        if root_person:
            related_persons.extend(root_person.get_filtered_related_persons(**kwargs))
        return related_persons
            

    def update_person(self, update_chidren_fingerprint=False):

        birth_date_data = ""
        birth_place_data = ""
        mother_data = ""
        father_data = ""

        if self.paternity_acknowledgment_date:
            self.birth_is_legitim = True

        curr_relationship = self.bride_in.first()
        if curr_relationship:
            self.husband_surname = curr_relationship.groom.father_surname


        try:
            birth_date_data = str(self.birth_date.year)
        except Exception as e:
            pass
            #print(e)

        try:
            birth_place_data = (self.birth_place.country or "") + (self.birth_place.state or "")
        except Exception as e:
            pass
            #print(e)

        try:
            mother_data = self.mother.name
        except Exception as e:
            pass
            #print(e)
        
        try:
            father_data = self.father.name
        except Exception as e:
            pass
        
        new_fingerprint = ((self.name or "") + father_data +"_"+ mother_data + birth_date_data + birth_place_data) or ""
        old_fingerprint = str(self.fingerprint)
        if self.fingerprint != new_fingerprint:
            self.fingerprint = new_fingerprint
            self.save(update_fingerprint=False)
            print(f"Fingerprint Updated for: {self.name} {self.father_surname} {self.mother_surname} , old fingerprint: <{old_fingerprint}> new fingerprint: <{new_fingerprint}>")
            if update_chidren_fingerprint:
                for child in self.mother_of.all():
                    child.update_person()
                for child in self.father_of.all():
                    child.update_person()


    def save(self, *args, update_fingerprint=True, **kwargs):

        updating = False
        name = ""
        update_chidren_fingerprint = False
        current_request = CrequestMiddleware.get_request()
        if bool(self.id):
            updating = True
            if current_request and not current_request.user.is_superuser and current_request.user != self.owner:
                return
            old_person = self.__class__.objects.get(pk=self.pk)
            name = str(old_person.name)
            del old_person
        else:
            if current_request and not current_request.user.is_superuser:
                self.owner = current_request.user
        super(Person, self).save(*args, **kwargs)
        self.refresh_from_db()
        if update_fingerprint:
            if updating and (name != str(self.name)):
                update_chidren_fingerprint = True
            self.update_person(update_chidren_fingerprint=update_chidren_fingerprint)

    class Meta:
        ordering = ["-id"]

class Relationship(models.Model):
    groom = models.ForeignKey(Person, default=None, null=True, blank=True, related_name="groom_in", on_delete=models.CASCADE)
    bride = models.ForeignKey(Person, default=None, null=True, blank=True, related_name="bride_in", on_delete=models.CASCADE)
    #children = models.ForeignKey(Person, null=True, related_name="legitimated_on_marriage", on_delete=models.CASCADE, help_text="Children legitimized by the Marriage.")
    marriage_witnesses = models.ManyToManyField(Person, related_name="marriage_witness_of")
    #marriage_sources = models.ManyToManyField(Source, related_name="marriages")
    marriage_places = models.ManyToManyField(Place, related_name="marriages")
    marriage_godmother = models.ForeignKey(Person, default=None, null=True, blank=True, related_name="godmother_on_wedding", on_delete=models.CASCADE, help_text="GodMother")
    marriage_godfather = models.ForeignKey(Person, default=None, null=True, blank=True, related_name="godfather_on_wedding", on_delete=models.CASCADE, help_text="GodFather")
    marriage_authorizers = models.ManyToManyField(Person, related_name="marriage_authorizer_of")
    properties = JSONField(null=True, blank=True, help_text="")
    start_date = models.DateField(null=True)
    end_date = models.DateField(null=True)
    comments = models.TextField(null=True, blank=True)
    is_hidden = models.BooleanField(default=False, help_text="True if marriage is hidden.")
    is_blocked = models.BooleanField(default=False, help_text="Prevent others to make changes to this marriage")
    owner = models.ForeignKey(User, default=None, null=True, blank=True, related_name="marriages", on_delete=models.CASCADE)
    is_deleted = models.BooleanField(default=False, help_text="True if the marriage is deleted.")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        current_request = CrequestMiddleware.get_request()
        if bool(self.id) and current_request and not current_request.user.is_superuser and current_request.user != self.owner:
            return
        if not bool(self.id) and current_request and not current_request.user.is_superuser:
            self.owner = current_request.user
        super(Relationship, self).save(*args, **kwargs)

    def __str__ (self):
        return f"Relationship({self.groom.name} and {self.bride.name}, on {self.start_date})"

    class Meta:
        ordering = ["-id"]


class Residence(models.Model):
    residents = models.ManyToManyField(Person, related_name="residences")
    residence_place = models.ForeignKey(Place, default=None, null=True, blank=True, related_name="residences", on_delete=models.CASCADE)
    comments = models.TextField(null=True, blank=True)
    is_deleted = models.BooleanField(default=False, help_text="True if the residence is deleted.")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    properties = JSONField(null=True, blank=True, help_text="")

    def __str__ (self):
        return f"Residence({self.residence_place})"

    class Meta:
        ordering = ["-id"]



class Emigration(models.Model):
    person = models.ForeignKey(Person, default=None, null=True, blank=True, related_name="emigrations", on_delete=models.CASCADE)
    _from = models.ForeignKey(Place, default=None, null=True, blank=True, related_name="emigrations", on_delete=models.CASCADE)
    to = models.ForeignKey(Place, default=None, null=True, blank=True, related_name="inmigrations", on_delete=models.CASCADE)
    properties = JSONField(null=True, blank=True, help_text="")
    date = models.DateField(null=True)
    comments = models.TextField(null=True, blank=True)
    is_hidden = models.BooleanField(default=False, help_text="True if emigration is hidden.")
    is_blocked = models.BooleanField(default=False, help_text="Prevent others to make changes to this emigration")
    owner = models.ForeignKey(User, default=None, null=True, blank=True, related_name="emigrations", on_delete=models.CASCADE)
    is_deleted = models.BooleanField(default=False, help_text="True if emigration is deleted.")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        current_request = CrequestMiddleware.get_request()
        if bool(self.id) and current_request and not current_request.user.is_superuser and current_request.user != self.owner:
            return
        if not bool(self.id) and current_request and not current_request.user.is_superuser:
            self.owner = current_request.user
        super(Emigration, self).save(*args, **kwargs)

    def __str__ (self):
        return f"Emigration({self.person} from {self._from}, to {self.to})"

    class Meta:
        ordering = ["-id"]


class Tag(models.Model):
    # main_male_legitim, main_female_legitim
    # father, father_legitim
    # bride_legitim, groom_legitim, bride, groom
    # son__N1, dauther__N2, godmother, GodFather, witness__N1, witness__N2, canonical_auth, civil_auth
    # declarant_female_legitim declarant_ authority_legitim
    kindred_type = models.TextField(null=True, blank=True)
    registry_number = models.IntegerField(null=True, blank=True)
    source = models.ForeignKey(Source, default=None, null=True, blank=True, related_name="tags", on_delete=models.CASCADE)
    person = models.ForeignKey(Person, default=None, null=True, blank=True, related_name="tags", on_delete=models.CASCADE)
    residence = models.ForeignKey(Residence, default=None, null=True, blank=True, related_name="tags", on_delete=models.CASCADE)
    place = models.ForeignKey(Place, default=None, null=True, blank=True, related_name="tags", on_delete=models.CASCADE)
    person_area = models.TextField(null=True, blank=True)
    person_age = models.TextField(null=True, blank=True)
    person_age_area = models.TextField(null=True, blank=True)
    place_area = models.TextField(null=True, blank=True)
    residence_area = models.TextField(null=True, blank=True)
    tag_date_area = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_deleted = models.BooleanField(default=False, help_text="True if deleted.")
    comments = models.TextField(null=True, blank=True)
    properties = JSONField(null=True, blank=True, help_text="")

    def __str__ (self):
        return f"Tag({self.person} in {self.source} {self.residence})"

    class Meta:
        ordering = ["-id"]

used_tz = timezone("America/Santo_Domingo")
#used_tz = timezone("UTC")


def dt(*arg, **kwargs):
    kwargs.setdefault("tzinfo", used_tz)
    return datetime(*arg, **kwargs)

def get_fingerprint(**kwargs):
    birth_date_data = kwargs.get("birth_date", "")
    if birth_date_data:
        birth_date_data = str(birth_data.year)
    birth_place_data = kwargs.get("birth_place", "")
    if birth_place_data:
        birth_place_data = birth_place_data.country or "" + birth_place_data.state or ""
    mother_data = kwargs.get("mother", "")
    if mother_data:
        mother_data = mother_data.birth_name or ""
    father_data = kwargs.get("father", "")
    if father_data:
        father_data = father_data.birth_name or ""
    return kwargs.get("birth_name") or "" + father_data + "_" + mother_data + birth_date_data + birth_place_data

def make_match(**kwargs):
    try:
        marriage_witnesses = kwargs.pop("marriage_witnesses")
    except:
        marriage_witnesses = []
    match = Relationship.objects.create(**kwargs)
    match.marriage_witnesses.set(marriage_witnesses)

    try:
        marriage_places = kwargs.pop("marriage_places")
    except:
        marriage_places = []
    match.marriage_places.set(marriage_places)
    #kwargs["groom"], kwargs["bride"] = kwargs["bride"], kwargs["groom"]
    #match = Relationship.objects.create(**kwargs)
    #match.marriage_witnesses.set(marriage_witnesses)
    return match


def make_person(match=None, validate_only=False, **kwargs):
    kwargs["birth_properties"] = {"test": "test"}
    if match:
        kwargs["father"] = match.groom
        kwargs["mother"] = match.bride

    if validate_only:
        return kwargs

    try:
        death_witnesses = kwargs.pop("death_witnesses")
    except:
        death_witnesses = []
    try:
        birth_witnesses = kwargs.pop("birth_witnesses")
    except:
        birth_witnesses = []
    try:
        baptism_witnesses = kwargs.pop("baptism_witnesses")
    except:
        baptism_witnesses = []

    #kwargs["fingerprint"] = get_fingerprint(**kwargs)
    person = Person.objects.create(**kwargs)
    person.death_witnesses.set(death_witnesses)
    person.birth_witnesses.set(birth_witnesses)
    person.baptism_witnesses.set(baptism_witnesses)
    return person
