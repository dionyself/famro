from django.db.models.manager import Manager
from django.db.models.query import QuerySet
from rest_framework import serializers

class DatabaseBackup(object):
    def __init__(self, **kwargs):
        for field in ('id', 'host', 'db_name', 'username', 'password', 'filename', 'download_url'):
            setattr(self, field, kwargs.get(field, None))
    def filter(self, *x, **Y):
        return self
    def all(self, *x, **Y):
        return self
    def first(self, *x, **Y):
        return self
    def last(self, *x, **Y):
        return self
class L(list):
    def __new__(self, *args, **kwargs):
        return super(L, self).__new__(self, args, kwargs)
    def __init__(self, *args, **kwargs):
        if len(args) == 1 and hasattr(args[0], '__iter__'):
            list.__init__(self, args[0])
        else:
            list.__init__(self, args)
        self.__dict__.update(kwargs)
    def __call__(self, **kwargs):
        self.__dict__.update(kwargs)
        return self
class DynamicField(serializers.HyperlinkedRelatedField):
    def get_queryset(self):
        queryset = self.queryset
        if hasattr(queryset, '__call__'):
            queryset = queryset(self)
        if isinstance(queryset, (QuerySet, Manager)):
            queryset = queryset.all()
        elif type(queryset) is list:
            queryset = L(*queryset)
            queryset.get = self.queryset.get
        return queryset
