from string import Formatter
from fpdf import Template
from rest_framework.reverse import reverse
from rest_framework import renderers
from django.template import engines, loader
from rest_framework import VERSION, exceptions, serializers, status
from famtree.models import Person, Place, Source, Tag, Relationship, Residence, PDFTemplate


class CustomBrowsableAPIRenderer(renderers.BrowsableAPIRenderer):

    def get_default_renderer(self, view):
        return renderers.JSONRenderer()

    def render(self, data, accepted_media_type=None, renderer_context=None):
        """
        Render the HTML for the browsable API representation.
        """
        self.accepted_media_type = accepted_media_type or ''
        self.renderer_context = renderer_context or {}

        try:
            template = loader.get_template(renderer_context['view'].template_name)
        except:
            print("View does not implement template_name")
            template = loader.get_template(self.template)

        context = self.get_context(data, accepted_media_type, renderer_context)
        try:
            context = renderer_context['view'].extend_template_context(
                template_context=context, data=data, renderer_context=renderer_context)
        except:
            print("View does not implement extend_template_context()")
        ret = template.render(context, request=renderer_context['request'])

        response = renderer_context['response']
        if response.status_code == status.HTTP_204_NO_CONTENT:
            response.status_code = status.HTTP_200_OK

        return ret


def get_menu(request=None, extra_links={}, exclude=(), include=()):
    default_menu = {
        "Persons": "/persons/",
        "Marriages": "/relationships/",
        "Emigrations": "/emigrations/",
        "Mentions": "/tags/",
        "Places": "/places/",
        "Residences": "/residences/",
        "Sources": "/sources/",
        "Trees": "/tree/",
        "Backups": "/backup/",
        "Logout": "/logout/",
    }
    default_menu.update(extra_links)
    menu_excluded = {
        "api":"/graphql" 
    }
    for key in exclude:
        del default_menu[key]
    for key in include:
        default_menu[key] = menu_excluded[key]
    return default_menu

class UnseenFormatter(Formatter):
    def get_value(self, key, args, kwds):
        if isinstance(key, str):
            try:
                return kwds[key]
            except KeyError:
                return "{" + key + "}"
        else:
            return Formatter.get_value(key, args, kwds)

def _replacer(key, template, replacer_type):
    place_holder = "_" * template.space_mapper[key]
    if len(place_holder) <= len(replacer_type):
        return replacer_type
    slace_start = len(place_holder)/3
    place_holder[slace_start:len(replacer_type)] = replacer_type
    return replacer_type

class ReplaceFormatter(Formatter):

    def __init__(self, pdf_template, replacer_type, replacer=(lambda x, y, z: x), *args, **kwds):
        super(Formatter, self).__init__(*args, **kwds)
        self.replacer = replacer
        self.pdf_template = pdf_template
        self.replacer_type = replacer_type
        
    def get_value(self, key, args, kwds):
        if isinstance(key, str):
            try:
                return kwds[key]
            except KeyError:
                return self.replacer(key, self.pdf_template, self.replacer_type)
        else:
            return Formatter.get_value(key, args, kwds)
            
fmt = UnseenFormatter()


def source_to_metadata(source, registry_number, use_template=""):
    result = {
        "source_type": use_template or source.source_type or "",
        "source_type_name": source.source_type_name or "",
        "source_container_name": source.properties.get("container_name", "") if source.properties else "",
        "source_container_number": source.properties.get("container_number", "") if source.properties else "",
        "source_page_number": source.properties.get("folio", "") if source.properties else "",
        "registry_number": registry_number or "",
        "source_place": source.source_place.state if source.source_place else "",
        "source_house": source.source_place.house if source.source_place else "",
        "source_date": source.source_date or "",
    }
    for tag in source.tags.filter(registry_number=registry_number).all():
        last_name = tag.person.mother_surname
        if "_legitim" in tag.kindred_type:
            last_name = tag.person.father_surname
            _ = tag.kindred_type.pop("_legitim", "")
        result.update({tag.kindred_type: {
            "name": tag.person.name,
            "full_name": tag.person.get_full_name(),
            "partial_name": f"{tag.person.name} {last_name}",
            "resident": tag.residence,
            "born_on": tag.person.birth_date,
            "birth_place": tag.person.birth_place.state if tag.person.birth_place else "",
            "age": tag.person_age}
        })
    return result

def metadata_to_pdf(metadata, use_spacer):
    pdf_template = PDFTemplate.objects.get(source_type=metadata["source_type"])
    data_mapper = pdf_template.mapper or {}
    page_title = pdf_template.title or ""
    pdf_body = pdf_template.body or []
    
    for key_name, fragments in data_mapper.items():
        if type(fragments) is int:
            pdf_body[fragments]["text"] = fmt.format(pdf_body[fragments]["text"], **{"{}".format(key_name): metadata[key_name]})
            continue
        if type(fragments) is list:
            for body_index in fragments:
                pdf_body[body_index]["text"] = fmt.format(pdf_body[body_index]['text'], **{"{}".format(key_name): metadata[key_name]})
            continue
        if type(fragments) is dict:
            for fragment, body_index in fragments.items():
                if type(body_index) is int:
                    pdf_body[body_index]["text"] = fmt.format(pdf_body[body_index]['text'], **{"{}_{}".format(key_name, fragment): metadata[key_name][fragment]})
                    continue
                if type(body_index) is list:
                    for body_sub_index in body_index:
                        pdf_body[body_sub_index]["text"] = fmt.format(pdf_body[body_sub_index]['text'], **{"{}_{}".format(key_name, fragment): metadata[key_name][fragment]})
                    continue
            continue
    
    page_title = fmt.format(page_title,
        main_male=metadata.get("main_male_legitim", ""),
        main_male_legitim=metadata.get("main_male_legitim", ""),
        main_female=metadata.get("main_female_legitim", ""),
        main_female_legitim=metadata.get("main_female_legitim", ""))
    replacer_formatter = ReplaceFormatter(pdf_template, use_spacer, replacer=_replacer)
    page_title = replacer_formatter.format(page_title)
    for key in pdf_body:
        pdf_body[key]["text"] = replacer_formatter.format(pdf_body[key]["text"])
    file_obj = Template(format="A4", elements=pdf_body, title=page_title)
    file_obj.add_page()
    return file_obj.render("./template.pdf", dest="S")
