import graphene
from graphene import relay
from graphene import ObjectType
from graphene_django import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField
from famtree import models
from graphene.types.generic import GenericScalar
from graphene.types.datetime import Date


class PlaceNode(DjangoObjectType):

    properties = GenericScalar()

    class Meta:
        model = models.Place
        exclude_fields = ["is_deleted"]
        interfaces = (relay.Node,)
        filter_fields = {
            "comments": ["icontains", "istartswith"],
            "country": ["in", "exact", "icontains", "istartswith"],
            "state": ["in", "exact", "icontains", "istartswith"],
            "city": ["in", "exact", "icontains", "istartswith"],
            "neighborhood": ["in", "exact", "icontains", "istartswith"],
            "house": ["in", "exact", "icontains", "istartswith"],
            "street": ["in", "exact", "icontains", "istartswith"],
            "place_type": ["in", "exact", "icontains", "istartswith"],
            "created_at": ["lte", "gte", "gt", "lt"],
            "updated_at": ["lte", "gte", "gt", "lt"],
            "is_deleted": ["exact"],
        }

class SourceNode(DjangoObjectType):

    properties = GenericScalar()

    class Meta:
        model = models.Source
        exclude_fields = ["is_deleted"]
        interfaces = (relay.Node,)
        filter_fields = {
            "name": ["in", "exact", "icontains", "istartswith"],
            "comments": ["icontains", "istartswith"],
            "url": ["in", "exact", "icontains", "istartswith"],
            "source_type": ["in", "exact", "icontains", "istartswith"],
            "source_type_name": ["in", "exact", "icontains", "istartswith"],
            "source_date": ["lte", "gte", "gt", "lt"],
            "source_place__state": ["in", "exact", "icontains", "istartswith"],
            "created_at": ["lte", "gte", "gt", "lt"],
            "updated_at": ["lte", "gte", "gt", "lt"],
            "is_deleted": ["exact"],
        }


class EmigrationNode(DjangoObjectType):

    properties = GenericScalar()

    class Meta:
        model = models.Emigration
        exclude_fields = ["is_deleted"]
        interfaces = (relay.Node,)
        filter_fields = {
            "comments": ["icontains", "istartswith"],
        }


class TagNode(DjangoObjectType):

    properties = GenericScalar()

    class Meta:
        model = models.Tag
        exclude_fields = ["is_deleted"]
        interfaces = (relay.Node,)
        filter_fields = {
            "comments": ["icontains", "istartswith"],
        }

class ResidenceNode(DjangoObjectType):

    properties = GenericScalar()

    class Meta:
        model = models.Residence
        exclude_fields = ["is_deleted"]
        interfaces = (relay.Node,)
        filter_fields = {
            "comments": ["icontains", "istartswith"],
        }

class PersonNode(DjangoObjectType):

    birth_properties = GenericScalar()
    death_properties = GenericScalar()
    baptism_properties = GenericScalar()
    #tree_node = graphene.String()

    tree_node = GenericScalar()
    def resolve_tree_node(self, info, **args):
        return self.get_tree_node(raw=True)

    children = DjangoFilterConnectionField(lambda *x, **y: PersonNode)
    def resolve_children(self, info, **args):
        return self.children

    emigrations = DjangoFilterConnectionField(lambda *x, **y: EmigrationNode)
    def resolve_emigrations(self, info, **args):
        return self.emigrations

    class Meta:
        model = models.Person
        exclude_fields = ["mother_set"]
        interfaces = (relay.Node,)
        filter_fields = {
            "name": ["in", "exact", "icontains", "istartswith"],
            "comments": ["icontains", "istartswith"],
            "fingerprint": ["in", "exact", "icontains", "istartswith"],
            "career": ["in", "exact", "icontains", "istartswith"],
            "mother__name": ["in", "exact", "icontains", "istartswith"],
            "father__name": ["in", "exact", "icontains", "istartswith"],
            "husband_surname": ["in", "exact", "icontains", "istartswith"],

            #"paternity_acknowledgment_source": ["exact"],
            "paternity_acknowledgment_date": ["lte", "gte", "gt", "lt"],
            "paternity_acknowledgment_place": ["exact"],
            "paternity_acknowledgment_witnesses__name": ["in", "exact", "icontains", "istartswith"],
            "paternity_acknowledgment_witnesses__father_surname": ["in", "exact", "icontains", "istartswith"],
            "paternity_acknowledgment_witnesses__mother_surname": ["in", "exact", "icontains", "istartswith"],
            "paternity_acknowledgment_authorized_by__name": ["in", "exact", "icontains", "istartswith"],
            "paternity_acknowledgment_authorized_by__father_surname": ["in", "exact", "icontains", "istartswith"],
            "paternity_acknowledgment_authorized_by__mother_surname": ["in", "exact", "icontains", "istartswith"],
            "acknowledged_on_marriage": ["exact"],
            "sources": ["exact"],

            "father_surname": ["in", "exact", "icontains", "istartswith"],
            "mother_surname": ["in", "exact", "icontains", "istartswith"],
            "birth_is_legitim": ["exact"],
            "birth_date": ["lte", "gte", "gt", "lt"],
            "birth_declared_by__name": ["in", "exact", "icontains", "istartswith"],
            "birth_declared_by__father_surname": ["in", "exact", "icontains", "istartswith"],
            "birth_declared_by__mother_surname": ["in", "exact", "icontains", "istartswith"],
            "birth_place": ["exact"],
            "birth_witnesses__name": ["in", "exact", "icontains", "istartswith"],
            "birth_witnesses__father_surname": ["in", "exact", "icontains", "istartswith"],
            "birth_witnesses__mother_surname": ["in", "exact", "icontains", "istartswith"],
            "birth_authorized_by__name": ["in", "exact", "icontains", "istartswith"],
            "birth_authorized_by__father_surname": ["in", "exact", "icontains", "istartswith"],
            "birth_authorized_by__mother_surname": ["in", "exact", "icontains", "istartswith"],
            #"birth_source": ["exact"],
            "birth_godmother__name": ["in", "exact", "icontains", "istartswith"],
            "birth_godmother__mother_surname": ["in", "exact", "icontains", "istartswith"],
            "birth_godmother__father_surname": ["in", "exact", "icontains", "istartswith"],
            "birth_godfather__name": ["in", "exact", "icontains", "istartswith"],
            "birth_godfather__father_surname": ["in", "exact", "icontains", "istartswith"],
            "birth_godfather__mother_surname": ["in", "exact", "icontains", "istartswith"],

            "baptism_authorized_by__name": ["in", "exact", "icontains", "istartswith"],
            "baptism_authorized_by__mother_surname": ["in", "exact", "icontains", "istartswith"],
            "baptism_authorized_by__father_surname": ["in", "exact", "icontains", "istartswith"],
            "baptism_date": ["lte", "gte", "gt", "lt"],
            "baptism_place": ["exact"],
            "baptism_witnesses__name": ["in", "exact", "icontains", "istartswith"],
            "baptism_witnesses__father_surname": ["in", "exact", "icontains", "istartswith"],
            "baptism_witnesses__mother_surname": ["in", "exact", "icontains", "istartswith"],
            #"baptism_source": ["exact"],
            "baptism_godmother__name": ["in", "exact", "icontains", "istartswith"],
            "baptism_godmother__father_surname": ["in", "exact", "icontains", "istartswith"],
            "baptism_godmother__mother_surname": ["in", "exact", "icontains", "istartswith"],
            "baptism_godfather__name": ["in", "exact", "icontains", "istartswith"],
            "baptism_godfather__father_surname": ["in", "exact", "icontains", "istartswith"],
            "baptism_godfather__mother_surname": ["in", "exact", "icontains", "istartswith"],

            "death_authorized_by__name": ["in", "exact", "icontains", "istartswith"],
            "death_authorized_by__father_surname": ["in", "exact", "icontains", "istartswith"],
            "death_authorized_by__mother_surname": ["in", "exact", "icontains", "istartswith"],
            "death_declared_by__name": ["in", "exact", "icontains", "istartswith"],
            "death_declared_by__father_surname": ["in", "exact", "icontains", "istartswith"],
            "death_declared_by__mother_surname": ["in", "exact", "icontains", "istartswith"],
            "death_witnesses__name": ["in", "exact", "icontains", "istartswith"],
            "death_witnesses__father_surname": ["in", "exact", "icontains", "istartswith"],
            "death_witnesses__mother_surname": ["in", "exact", "icontains", "istartswith"],
            "death_place": ["exact"],
            "death_date": ["lte", "gte", "gt", "lt"],
            #"death_sources": ["exact"],
            "is_surname_freezed": ["exact"],
            "is_deleted": ["exact"],
            "created_at": ["lte", "gte", "gt", "lt"],
            "updated_at": ["lte", "gte", "gt", "lt"],
        }


class MarriageNode(DjangoObjectType):

    properties = GenericScalar()

    class Meta:
        model = models.Relationship
        exclude_fields = ["mother_set"]
        interfaces = (relay.Node,)
        filter_fields = {
            "groom__name": ["in", "exact", "icontains", "istartswith"],
            "bride__name": ["in", "exact", "icontains", "istartswith"],
            "comments": ["icontains", "istartswith"],
        }


class PlaceEditor(graphene.Mutation):
    class Arguments:

        pk = graphene.Int()
        country = graphene.String()
        state = graphene.String()
        city = graphene.String()
        neighborhood = graphene.String()
        house = graphene.String()
        place_type = graphene.String()
        street = graphene.String()
        properties = GenericScalar(description="")
        comments = graphene.String()
        is_deleted = graphene.Boolean()

    place_id = graphene.Int()
    updated_fields = GenericScalar(description="")
    errors = GenericScalar(description="")
    ok = graphene.Boolean()

    def mutate(self, info, **args):

        pk = args.pop("pk", 0)
        result= {
            "ok": False,
            "place_id": pk,
            "updated_fields": {},
            "errors": {}
        }

        if pk and not args:
            args["is_deleted"] = True
            place = models.Place.objects.filter(id=pk).first()
            place.__dict__.update(**args)
            place.save()
            result["updated_fields"]["is_deleted"] = [place.is_deleted, True]
            result["ok"] = True
            return PlaceEditor(**result)
        elif not args:
            result["error"]["request"] = "Empty request"
            return PlaceEditor(**result)

        parsed_args = args.copy()

        try:
            if pk:
                place = models.Place.objects.filter(id=pk).first()
                for key, value in parsed_args.items():
                    result["updated_fields"][key] = [getattr(place, key)]
                place.__dict__.update(**parsed_args)
                place.save()
                for key, value in parsed_args.items():
                    result["updated_fields"][key].append(value)
                result["ok"] = True
            else:
                place = models.Place.objects.create(**parsed_args)
                for key, value in parsed_args.items():
                    result["updated_fields"][key] = [None, value]
                result["ok"] = True
                result["place_id"] = place.id
        except Exception as e:
            result["place_id"] = 0
            result["errors"]["exception"] = str(e)


        return PlaceEditor(**result)


class SourceEditor(graphene.Mutation):
    class Arguments:

        pk = graphene.Int()
        name = graphene.String()
        url = graphene.String()
        properties = GenericScalar(description="")
        source_date = Date()
        source_place_id = graphene.Int()
        is_deleted = graphene.Boolean()
        properties = GenericScalar(description="")
        comments = graphene.String()
        persons_to_add = graphene.List(graphene.Int)
        persons_to_remove = graphene.List(graphene.Int)

    source_id = graphene.Int()
    updated_fields = GenericScalar(description="")
    errors = GenericScalar(description="")
    ok = graphene.Boolean()

    def mutate(self, info, **args):

        pk = args.pop("pk", 0)
        persons_to_add = args.pop("persons_to_add", [])
        persons_to_remove = args.pop("persons_to_remove", [])
        result = {
            "ok": False,
            "source_id": pk,
            "updated_fields": {},
            "errors": {},
            "added_persons": [],
            "removed_persons": [],
            "failed_persons": [],
        }

        if pk and not args:
            args["is_deleted"] = True
            source = models.Source.objects.filter(id=pk).first()
            source.__dict__.update(**args)
            source.save()
            result["updated_fields"]["is_deleted"] = [source.is_deleted, True]
            result["ok"] = True
            return SourceEditor(**result)
        elif not args:
            result["error"]["request"] = "Empty request"
            return SourceEditor(**result)

        parsed_args = args.copy()

        try:
            if pk:
                source = models.Source.objects.filter(id=pk).first()
                for key, value in parsed_args.items():
                    result["updated_fields"][key] = [getattr(source, key)]
                source.__dict__.update(**parsed_args)
                source.save()
                for key, value in parsed_args.items():
                    result["updated_fields"][key].append(value)
                result["ok"] = True
            else:
                source = models.Source.objects.create(**parsed_args)
                for key, value in parsed_args.items():
                    result["updated_fields"][key] = [None, value]
                result["ok"] = True
                result["source_id"] = source.id
        except Exception as e:
            result["source_id"] = 0
            result["errors"]["exception"] = str(e)

        for person_id in persons_to_add:
            try:
                person = models.Person.objects.get(pk=person_id)
                source.persons.add(person)
                result["added_persons"].append(person_id)
            except Exception as e:
                result["failed_persons"].append(person_id)
                result["errors"]["exception"] = str(e)
                result["ok"] = False
        for person_id in persons_to_remove:
            try:
                source.persons.remove(models.Person.objects.get(pk=person_id))
                result["removed_persons"].append(person_id)
            except Exception as e:
                result["failed_persons"].append(person_id)
                result["errors"]["exception"] = str(e)
                result["ok"] = False


        return SourceEditor(**result)


class TagEditor(graphene.Mutation):
    class Arguments:

        pk = graphene.Int()
        source_id = graphene.Int()
        person_id = graphene.Int()
        place_id = graphene.Int()
        residence_id = graphene.Int()
        tag_date = Date()
        place_area = graphene.String()
        residence_area = graphene.String()
        tag_date_area = graphene.String()
        person_area = graphene.String()
        is_deleted = graphene.Boolean()
        comments = graphene.String()
        properties = GenericScalar(description="")

    tag_id = graphene.Int()
    updated_fields = GenericScalar(description="")
    errors = GenericScalar(description="")
    ok = graphene.Boolean()

    def mutate(self, info, **args):

        pk = args.pop("pk", 0)
        result= {
            "ok": False,
            "tag_id": pk,
            "updated_fields": {},
            "errors": {}
        }

        if pk and not args:
            args["is_deleted"] = True
            tag = models.Tag.objects.filter(id=pk).first()
            tag.__dict__.update(**args)
            tag.save()
            result["updated_fields"]["is_deleted"] = [tag.is_deleted, True]
            result["ok"] = True
            return TagEditor(**result)
        elif not args:
            result["error"]["request"] = "Empty request"
            return TagEditor(**result)

        parsed_args = args.copy()

        try:
            if pk:
                tag = models.Tag.objects.filter(id=pk).first()
                for key, value in parsed_args.items():
                    result["updated_fields"][key] = [getattr(tag, key)]
                tag.__dict__.update(**parsed_args)
                tag.save()
                for key, value in parsed_args.items():
                    result["updated_fields"][key].append(value)
                result["ok"] = True
            else:
                tag = models.Tag.objects.create(**parsed_args)
                for key, value in parsed_args.items():
                    result["updated_fields"][key] = [None, value]
                result["ok"] = True
                result["tag_id"] = tag.id
        except Exception as e:
            result["tag_id"] = 0
            result["errors"]["exception"] = str(e)


        return TagEditor(**result)


class ResidenceEditor(graphene.Mutation):
    class Arguments:

        pk = graphene.Int()
        residence_place_id = graphene.Int()
        is_deleted = graphene.Boolean()
        comments = graphene.String()
        properties = GenericScalar(description="")

    residence_id = graphene.Int()
    updated_fields = GenericScalar(description="")
    errors = GenericScalar(description="")
    ok = graphene.Boolean()

    def mutate(self, info, **args):

        pk = args.pop("pk", 0)
        result= {
            "ok": False,
            "residence_id": pk,
            "updated_fields": {},
            "errors": {}
        }

        if pk and not args:
            args["is_deleted"] = True
            residence = models.Residence.objects.filter(id=pk).first()
            residence.__dict__.update(**args)
            tag.save()
            result["updated_fields"]["is_deleted"] = [residence.is_deleted, True]
            result["ok"] = True
            return ResidenceEditor(**result)
        elif not args:
            result["error"]["request"] = "Empty request"
            return ResidenceEditor(**result)

        parsed_args = args.copy()

        try:
            if pk:
                residence = models.Residence.objects.filter(id=pk).first()
                for key, value in parsed_args.items():
                    result["updated_fields"][key] = [getattr(tag, key)]
                residence.__dict__.update(**parsed_args)
                residence.save()
                for key, value in parsed_args.items():
                    result["updated_fields"][key].append(value)
                result["ok"] = True
            else:
                residence = models.Residence.objects.create(**parsed_args)
                for key, value in parsed_args.items():
                    result["updated_fields"][key] = [None, value]
                result["ok"] = True
                result["residence_id"] = residence.id
        except Exception as e:
            result["residence_id"] = 0
            result["errors"]["exception"] = str(e)

        return ResidenceEditor(**result)


class MarriageEditor(graphene.Mutation):
    class Arguments:

        pk = graphene.Int()
        groom_id = graphene.Int()
        bride_id = graphene.Int()
        children_to_add = graphene.List(graphene.Int, description="children legitimized on the marriage")
        children_to_remove = graphene.List(graphene.Int, description="children legitimized on the marriage")
        places_to_add = graphene.List(graphene.Int, description="Places of the Marriage")
        places_to_remove = graphene.List(graphene.Int, description="Places of the Marriage")
        marriage_witnesses_to_add = graphene.List(graphene.Int)
        marriage_witnesses_to_remove = graphene.List(graphene.Int)
        marriage_source_id = graphene.Int()
        marriage_place = graphene.String()
        marriage_authorizers_to_remove = graphene.List(graphene.Int)
        marriage_authorizers_to_add = graphene.List(graphene.Int)
        properties = GenericScalar(description="")
        start_date = Date()
        end_date = Date()
        comments = graphene.String()
        is_deleted = graphene.Boolean()

    marriage_id = graphene.Int()
    updated_fields = GenericScalar(description="")
    added_children = graphene.List(graphene.Int)
    removed_children = graphene.List(graphene.Int)
    added_places = graphene.List(graphene.Int)
    removed_places = graphene.List(graphene.Int)
    failed_children = graphene.List(graphene.Int)
    added_marriage_witnesses = graphene.List(graphene.Int)
    removed_marriage_witnesses = graphene.List(graphene.Int)
    failed_marriage_witnesses = graphene.List(graphene.Int)
    added_marriage_authorizers = graphene.List(graphene.Int)
    removed_marriage_authorizers = graphene.List(graphene.Int)
    failed_marriage_authorizers = graphene.List(graphene.Int)
    errors = GenericScalar(description="")
    ok = graphene.Boolean()

    def mutate(self, info, **args):

        pk = args.pop("pk", 0)
        result= {
            "ok": False,
            "marriage_id": pk,
            "updated_fields": {},
            "added_children": [],
            "removed_children": [],
            "failed_children": [],
            "added_marriage_witnesses": [],
            "removed_marriage_witnesses": [],
            "added_places": [],
            "removed_places": [],
            "failed_marriage_witnesses": [],
            "added_marriage_authorizers": [],
            "removed_marriage_authorizers": [],
            "failed_marriage_authorizers": [],
            "errors": {}
        }

        if pk and not args:
            args["is_deleted"] = True
            marriage = models.Relationship.objects.filter(id=pk).first()
            marriage.__dict__.update(**args)
            marriage.save()
            result["updated_fields"]["is_deleted"] = [marriage.is_deleted, True]
            result["ok"] = True
            return MarriageEditor(**result)
        elif not args:
            result["error"]["request"] = "Empty request"
            return MarriageEditor(**result)

        children_to_add = args.pop("children_to_add", [])
        children_to_remove = args.pop("children_to_remove", [])
        marriage_witnesses_to_add = args.pop("marriage_witnesses_to_add", [])
        marriage_witnesses_to_remove = args.pop("marriage_witnesses_to_remove", [])
        places_to_add = args.pop("marriage_witnesses_to_add", [])
        places_to_remove = args.pop("marriage_witnesses_to_remove", [])
        marriage_authorizers_to_add = args.pop("marriage_authorizers_to_add", [])
        marriage_authorizers_to_remove = args.pop("marriage_authorizers_to_remove", [])

        parsed_args = args.copy()

        try:
            if pk:
                marriage = models.Relationship.objects.filter(id=pk).first()
                for key, value in parsed_args.items():
                    result["updated_fields"][key] = [getattr(marriage, key)]
                marriage.__dict__.update(**parsed_args)
                marriage.save()
                for key, value in parsed_args.items():
                    result["updated_fields"][key].append(value)
                result["ok"] = True
            else:
                marriage = models.make_match(**parsed_args)
                for key, value in parsed_args.items():
                    result["updated_fields"][key] = [None, value]
                result["ok"] = True
                result["marriage_id"] = marriage.id
        except Exception as e:
            result["marriage_id"] = 0
            result["errors"]["exception"] = str(e)


        for child_id in children_to_add:
            try:
                child = models.Person.objects.get(pk=child_id)
                marriage.children.add(child)
                result["added_children"].append(child_id)
            except Exception as e:
                result["failed_children"].append(child_id)
                result["errors"]["exception"] = str(e)
                result["ok"] = False
        for child_id in children_to_remove:
            try:
                marriage.children.remove(
                    models.Person.objects.get(pk=child_id))
                result["removed_children"].append(child_id)
            except Exception as e:
                result["failed_children"].append(child_id)
                result["errors"]["exception"] = str(e)
                result["ok"] = False


        for marriage_witness_id in marriage_witnesses_to_add:
            try:
                marriage_witness = models.Person.objects.get(pk=marriage_witness_id)
                marriage.birth_witnesses.add(marriage_witness)
                result["added_marriage_witnesses"].append(marriage_witness_id)
            except Exception as e:
                result["failed_marriage_witnesses"].append(marriage_witness_id)
                result["errors"]["exception"] = str(e)
                result["ok"] = False
        for marriage_witness_id in marriage_witnesses_to_remove:
            try:
                marriage.marriage_witnesses.remove(
                    models.Person.objects.get(pk=marriage_witness_id))
                result["removed_marriage_witnesses"].append(marriage_witness_id)
            except Exception as e:
                result["failed_marriage_witnesses"].append(marriage_witness_id)
                result["errors"]["exception"] = str(e)
                result["ok"] = False


        for marriage_authorizer_id in marriage_authorizers_to_add:
            try:
                marriage_authorizer = models.Person.objects.get(pk=marriage_authorizer_id)
                marriage.marriage_authorizers.add(marriage_authorizer)
                result["added_marriage_authorizers"].append(marriage_authorizer_id)
            except Exception as e:
                result["failed_marriage_authorizers"].append(marriage_authorizer_id)
                result["errors"]["exception"] = str(e)
                result["ok"] = False
        for marriage_authorizer_id in marriage_authorizers_to_remove:
            try:
                marriage.marriage_authorizers.remove(
                    models.Person.objects.get(pk=marriage_authorizer_id))
                result["removed_marriage_authorizers"].append(marriage_authorizer_id)
            except Exception as e:
                result["failed_marriage_authorizers"].append(marriage_authorizer_id)
                result["errors"]["exception"] = str(e)
                result["ok"] = False

        for place_id in places_to_add:
            try:
                place = models.Place.objects.get(pk=place_id)
                marriage.marriage_places.add(place)
                result["added_places"].append(place_id)
            except Exception as e:
                result["failed_places"].append(place_id)
                result["errors"]["exception"] = str(e)
                result["ok"] = False
        for place_id in places_to_remove:
            try:
                marriage.marriage_places.remove(
                    models.Place.objects.get(pk=place_id))
                result["removed_placess"].append(place_id)
            except Exception as e:
                result["failed_places"].append(place_id)
                result["errors"]["exception"] = str(e)
                result["ok"] = False

        return MarriageEditor(**result)





class PersonEditor(graphene.Mutation):
    class Arguments:

        pk = graphene.Int()
        name = graphene.String(description="Optional Name, Appears if the Person or an event (paternity acknowledgment/legitimation) changed his name")
        last_name = graphene.String(description="Optional Name, Appears if the Person or an event (paternity acknowledgment/legitimation) changed his last name")
        career = graphene.String()
        parents_marriage_id = graphene.Int(description="Optional Parents`s marriage, if provided it will override father and mother")
        mother_id = graphene.Int()
        father_id = graphene.Int()
        sources_to_add = graphene.List(graphene.Int, description="sources")
        sources_to_remove = graphene.List(graphene.Int, description="sources")

        paternity_acknowledgment_date = Date()
        paternity_acknowledgment_properties = GenericScalar(description="Serial numbers, page ...")
        paternity_acknowledgment_witnesses_to_add = graphene.List(graphene.Int, description="witnesses")
        paternity_acknowledgment_witnesses_to_remove = graphene.List(graphene.Int, description="witnesses")
        paternity_acknowledgment_authorized_by_id = graphene.Int(description="Civil authority")

        birth_name = graphene.String(description="Required Name")
        birth_last_name = graphene.String(description="Required Last Name")
        birth_is_legitim = graphene.Boolean(description="True if birth under legal marriage.")
        birth_date = Date()
        birth_declared_by_id = graphene.Int()
        birth_place = graphene.String()
        birth_witnesses_to_add = graphene.List(graphene.Int)
        birth_witnesses_to_remove = graphene.List(graphene.Int)
        birth_authorized_by_id = graphene.Int()
        birth_properties = GenericScalar()

        baptism_authorized_by_id = graphene.Int()
        baptism_date = Date()
        baptism_place = graphene.String()
        baptism_witnesses_to_add = graphene.List(graphene.Int)
        baptism_witnesses_to_remove = graphene.List(graphene.Int)
        baptism_properties = GenericScalar()

        death_authorized_by_id = graphene.Int()
        death_declared_by_id = graphene.Int()
        death_witnesses_to_add = graphene.List(graphene.Int)
        death_witnesses_to_remove = graphene.List(graphene.Int)
        death_place = graphene.String()
        death_date = Date()
        death_properties = GenericScalar(description="")

        residences_to_add = graphene.List(graphene.Int)
        residences_to_remove = graphene.List(graphene.Int)
        properties = GenericScalar(description="")
        comments = graphene.String()
        is_surname_freezed = graphene.Boolean()
        is_deleted = graphene.Boolean()

    person_id = graphene.Int()
    updated_fields = GenericScalar(description="")
    added_sources = graphene.List(graphene.Int)
    removed_sources = graphene.List(graphene.Int)
    failed_sources = graphene.List(graphene.Int)
    added_paternity_acknowledgment_witnesses = graphene.List(graphene.Int)
    removed_paternity_acknowledgment_witnesses = graphene.List(graphene.Int)
    failed_paternity_acknowledgment_witnesses = graphene.List(graphene.Int)
    added_birth_witnesses = graphene.List(graphene.Int)
    removed_birth_witnesses = graphene.List(graphene.Int)
    failed_birth_witnesses = graphene.List(graphene.Int)
    added_baptism_witnesses = graphene.List(graphene.Int)
    removed_baptism_witnesses = graphene.List(graphene.Int)
    failed_baptism_witnesses = graphene.List(graphene.Int)
    added_death_witnesses = graphene.List(graphene.Int)
    removed_death_witnesses = graphene.List(graphene.Int)
    failed_death_witnesses = graphene.List(graphene.Int)
    added_residences = graphene.List(graphene.Int)
    removed_residences = graphene.List(graphene.Int)
    failed_residences = graphene.List(graphene.Int)
    errors = GenericScalar(description="")
    ok = graphene.Boolean()

    def mutate(self, info, **args):

        pk = args.pop("pk", 0)
        result= {
            "ok": False,
            "person_id": pk,
            "updated_fields": {},
            "added_sources": [],
            "removed_sources": [],
            "failed_sources": [],
            "added_paternity_acknowledgment_witnesses": [],
            "removed_paternity_acknowledgment_witnesses": [],
            "failed_paternity_acknowledgment_witnesses": [],
            "added_birth_witnesses": [],
            "removed_birth_witnesses": [],
            "failed_birth_witnesses": [],
            "added_baptism_witnesses": [],
            "removed_baptism_witnesses": [],
            "failed_baptism_witnesses": [],
            "added_death_witnesses": [],
            "removed_death_witnesses": [],
            "failed_death_witnesses": [],
            "added_residences": [],
            "removed_residences": [],
            "failed_residences": [],
            "errors": {}
        }

        if pk and not args:
            args["is_deleted"] = True
            person = models.Person.objects.filter(id=pk).first()
            person.__dict__.update(**args)
            person.save()
            result["updated_fields"]["is_deleted"] = [person.is_deleted, True]
            result["ok"] = True
            return PersonEditor(**result)
        elif not args:
            result["error"]["request"] = "Empty request"
            return PersonEditor(**result)

        sources_to_add = args.pop("sources_to_add", [])
        sources_to_remove = args.pop("sources_to_remove", [])
        paternity_acknowledgment_witnesses_to_add = args.pop("paternity_acknowledgment_witnesses_to_add", [])
        paternity_acknowledgment_witnesses_to_remove = args.pop("paternity_acknowledgment_witnesses_to_remove", [])
        birth_witnesses_to_add = args.pop("birth_witnesses_to_add", [])
        birth_witnesses_to_remove = args.pop("birth_witnesses_to_remove", [])
        baptism_witnesses_to_add = args.pop("baptism_witnesses_to_add", [])
        baptism_witnesses_to_remove = args.pop("baptism_witnesses_to_remove", [])
        death_witnesses_to_add = args.pop("death_witnesses_to_add", [])
        death_witnesses_to_remove = args.pop("death_witnesses_to_remove", [])
        residences_to_add = args.pop("residences_to_add", [])
        residences_to_remove = args.pop("residences_to_remove", [])
        parents_marriage_id = args.pop("parents_marriage_id", None)
        marriage = None
        if parents_marriage_id:
            marriage = models.Relationship.objects.filter(id=parents_marriage_id).first()
        parsed_args = args.copy()
        if marriage:
            parsed_args["match"] = marriage
            #parsed_args["mother_id"] = marriage.bride_id
            #parsed_args["father_id"] = marriage.groom_id

        try:
            if pk:
                person = models.Person.objects.filter(id=pk).first()
                for key, value in parsed_args.items():
                    result["updated_fields"][key] = [getattr(person, key)]
                person.__dict__.update(**parsed_args)
                person.save()
                for key, value in parsed_args.items():
                    result["updated_fields"][key].append(value)
                result["ok"] = True
            else:
                person = models.make_person(**parsed_args)
                for key, value in parsed_args.items():
                    result["updated_fields"][key] = [None, value]
                result["ok"] = True
                result["person_id"] = person.id
        except Exception as e:
            result["person_id"] = 0
            result["errors"]["exception"] = str(e)

        for source_id in sources_to_add:
            try:
                source = models.Source.objects.get(pk=source_id)
                person.sources.add(source)
                result["added_sources"].append(source_id)
            except Exception as e:
                result["failed_sources"].append(source_id)
                result["errors"]["exception"] = str(e)
                result["ok"] = False
        for source_id in sources_to_remove:
            try:
                person.sources.remove(models.Source.objects.get(pk=source_id))
                result["removed_sources"].append(source_id)
            except Exception as e:
                result["failed_sources"].append(source_id)
                result["errors"]["exception"] = str(e)
                result["ok"] = False

        for paternity_acknowledgment_witness_id in paternity_acknowledgment_witnesses_to_add:
            try:
                acknowledgment_witness = models.Person.objects.get(pk=paternity_acknowledgment_witness_id)
                person.paternity_acknowledgment_witnesses.add(paternity_acknowledgment_witness)
                result["added_paternity_acknowledgment_witnesses"].append(paternity_acknowledgment_witness_id)
            except Exception as e:
                result["failed_paternity_acknowledgment_witnesses"].append(paternity_acknowledgment_witness_id)
                result["errors"]["exception"] = str(e)
                result["ok"] = False
        for paternity_acknowledgment_witness_id in paternity_acknowledgment_witnesses_to_remove:
            try:
                person.paternity_acknowledgment_witnesses.remove(
                    models.Person.objects.get(pk=paternity_acknowledgment_witness_id))
                result["removed_paternity_acknowledgment_witnesses"].append(paternity_acknowledgment_witness_id)
            except Exception as e:
                result["failed_paternity_acknowledgment_witnesses"].append(paternity_acknowledgment_witness_id)
                result["errors"]["exception"] = str(e)
                result["ok"] = False

        for birth_witness_id in birth_witnesses_to_add:
            try:
                birth_witness = models.Person.objects.get(pk=birth_witness_id)
                person.birth_witnesses.add(birth_witness)
                result["added_birth_witnesses"].append(birth_witness_id)
            except Exception as e:
                result["failed_birth_witnesses"].append(birth_witness_id)
                result["errors"]["exception"] = str(e)
                result["ok"] = False
        for birth_witness_id in birth_witnesses_to_remove:
            try:
                person.birth_witnesses.remove(
                    models.Person.objects.get(pk=birth_witness_id))
                result["removed_birth_witnesses"].append(birth_witness_id)
            except Exception as e:
                result["failed_birth_witnesses"].append(birth_witness_id)
                result["errors"]["exception"] = str(e)
                result["ok"] = False

        for baptism_witness_id in baptism_witnesses_to_add:
            try:
                baptism_witness = models.Person.objects.get(pk=baptism_witness_id)
                person.baptism_witnesses.add(baptism_witness)
                result["added_baptism_witnesses"].append(baptism_witness_id)
            except Exception as e:
                result["failed_baptism_witnesses"].append(baptism_witness_id)
                result["errors"]["exception"] = str(e)
                result["ok"] = False
        for baptism_witness_id in baptism_witnesses_to_remove:
            try:
                person.baptism_witnesses.remove(
                    models.Person.objects.get(pk=baptism_witness_id))
                result["removed_baptism_witnesses"].append(baptism_witness_id)
            except Exception as e:
                result["failed_baptism_witnesses"].append(baptism_witness_id)
                result["errors"]["exception"] = str(e)
                result["ok"] = False

        for death_witness_id in death_witnesses_to_add:
            try:
                death_witness = models.Person.objects.get(pk=death_witness_id)
                person.death_witnesses.add(death_witness)
                result["added_death_witnesses"].append(death_witness_id)
            except Exception as e:
                result["failed_death_witnesses"].append(death_witness_id)
                result["errors"]["exception"] = str(e)
                result["ok"] = False
        for death_witness_id in death_witnesses_to_remove:
            try:
                person.death_witnesses.remove(
                    models.Person.objects.get(pk=death_witness_id))
                result["removed_death_witnesses"].append(death_witness_id)
            except Exception as e:
                result["failed_death_witnesses"].append(death_witness_id)
                result["errors"]["exception"] = str(e)
                result["ok"] = False

        for residence_id in residences_to_add:
            try:
                residence = models.Residence.objects.get(pk=residence_id)
                person.residences.add(residence)
                result["added_residences"].append(residence_id)
            except Exception as e:
                result["failed_residences"].append(residence_id)
                result["errors"]["exception"] = str(e)
                result["ok"] = False
        for residence_id in residences_to_remove:
            try:
                person.residences.remove(
                    models.Residence.objects.get(pk=residence_id))
                result["removed_residences"].append(residence_id)
            except Exception as e:
                result["failed_residences"].append(residence_id)
                result["errors"]["exception"] = str(e)
                result["ok"] = False

        return PersonEditor(**result)


class EmigrationEditor(graphene.Mutation):
    class Arguments:

        pk = graphene.Int()
        person_id = graphene.Int()
        _from_id = graphene.Int()
        to_id = graphene.Int()
        is_deleted = graphene.Boolean()
        comments = graphene.String()
        properties = GenericScalar(description="")

    emigration_id = graphene.Int()
    updated_fields = GenericScalar(description="")
    errors = GenericScalar(description="")
    ok = graphene.Boolean()

    def mutate(self, info, **args):

        pk = args.pop("pk", 0)
        result= {
            "ok": False,
            "emigration_id": pk,
            "updated_fields": {},
            "errors": {}
        }

        if pk and not args:
            args["is_deleted"] = True
            emigration = models.Emigration.objects.filter(id=pk).first()
            emigration.__dict__.update(**args)
            emigration.save()
            result["updated_fields"]["is_deleted"] = [emigration.is_deleted, True]
            result["ok"] = True
            return EmigrationEditor(**result)
        elif not args:
            result["error"]["request"] = "Empty request"
            return EmigrationEditor(**result)

        parsed_args = args.copy()

        try:
            if pk:
                emigration = models.Emigration.objects.filter(id=pk).first()
                for key, value in parsed_args.items():
                    result["updated_fields"][key] = [getattr(emigration, key)]
                emigration.__dict__.update(**parsed_args)
                emigration.save()
                for key, value in parsed_args.items():
                    result["updated_fields"][key].append(value)
                result["ok"] = True
            else:
                emigration = models.Emigration.objects.create(**parsed_args)
                for key, value in parsed_args.items():
                    result["updated_fields"][key] = [None, value]
                result["ok"] = True
                result["emigration_id"] = emigration.id
        except Exception as e:
            result["emigration_id"] = 0
            result["errors"]["exception"] = str(e)


        return EmigrationEditor(**result)


class Mutation(ObjectType):
    person_editor = PersonEditor.Field()
    marriage_editor = MarriageEditor.Field()
    place_editor = PlaceEditor.Field()
    source_editor = SourceEditor.Field()
    residence_editor = ResidenceEditor.Field()
    tag_editor = TagEditor.Field()
    emigration_editor = EmigrationEditor.Field()

class Query(object):
    Person = relay.Node.Field(PersonNode)
    Persons = DjangoFilterConnectionField(PersonNode)
    Residence = relay.Node.Field(ResidenceNode)
    Residences = DjangoFilterConnectionField(ResidenceNode)
    Marriage = relay.Node.Field(MarriageNode)
    Marriages = DjangoFilterConnectionField(MarriageNode)
    Place = relay.Node.Field(PlaceNode)
    Places = DjangoFilterConnectionField(PlaceNode)
    Source = relay.Node.Field(SourceNode)
    Sources = DjangoFilterConnectionField(SourceNode)
    Tag = relay.Node.Field(TagNode)
    Tags = DjangoFilterConnectionField(TagNode)
    Emigration = relay.Node.Field(EmigrationNode)
    Emigrations = DjangoFilterConnectionField(EmigrationNode)
