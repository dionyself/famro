from django.apps import AppConfig


class FamtreeConfig(AppConfig):
    name = 'famtree'
