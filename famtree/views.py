import os
import qrcode
import io
from collections import OrderedDict
from fpdf import Template
from django.shortcuts import render
from rest_framework import renderers
from rest_framework import viewsets, mixins
from rest_framework.response import Response
from rest_framework.reverse import reverse
from django.views import View
from django.views.generic import TemplateView
from django.views.generic.base import ContextMixin
from django.http import HttpResponse
from famtree.models import Person, Place, Source, Tag, Relationship, Residence, PDFTemplate, Emigration
from famtree.utils import source_to_metadata, metadata_to_pdf, get_menu, CustomBrowsableAPIRenderer
from famtree.serializers import DatabaseBackupSerializer, ResidenceSerializer, \
    PersonSerializer, PlaceSerializer, SourceSerializer, TagSerializer, \
    RelationshipSerializer, EmigrationSerializer
from django.contrib.sites.shortcuts import get_current_site
from sendfile import sendfile
from django.contrib.auth.mixins import LoginRequiredMixin

from rest_framework import status
from rest_framework.response import Response
from rest_framework.serializers import Serializer
#from rest_framework.views import APIView
from django.contrib.auth import logout
from django.views import View
from django.shortcuts import redirect
from rest_framework import routers
from collections import OrderedDict, namedtuple
from django.urls import NoReverseMatch
from rest_framework.reverse import reverse
from rest_framework.response import Response


class FamroBaseView():
    renderer_classes = (CustomBrowsableAPIRenderer,)
    template_name = "rest_framework/base.html"

    def extend_template_context(self, template_context={}, data={}, renderer_context={}):
        template_context['menu'] = get_menu(renderer_context['request'])
        return template_context



class LogoutViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):
    serializer_class = Serializer
    queryset = []

    def list(self, request):
        logout(self.request)
        return redirect("/")


class CustomAPIRootView(FamroBaseView, routers.APIRootView):
    pass


class BaseAuthViewSet(LoginRequiredMixin, FamroBaseView, viewsets.ModelViewSet):


    def list(self, request):
        return super().list(request)

    def create(self, request):
        if not request.user.is_superuser:
            return Response({'status': 'You do not have enough permissions to do that.'})
        return super().create(request)

    def retrieve(self, request, pk=None):
        return super().retrieve(request)

    def update(self, request, pk=None):
        if not request.user.is_superuser:
            return Response({'status': 'You do not have enough permissions to do that.'})
        return super().update(request)

    def partial_update(self, request, pk=None):
        if not request.user.is_superuser:
            return Response({'status': 'You do not have enough permissions to do that.'})
        return super().partial_update(request)

    def destroy(self, request, pk=None):
        if not request.user.is_superuser:
            return Response({'status': 'You do not have enough permissions to do that.'})
        return super().destroy(request)


class ResidenceViewSet(BaseAuthViewSet):
    queryset = Residence.objects.all()
    serializer_class = ResidenceSerializer

class PersonViewSet(LoginRequiredMixin, FamroBaseView, viewsets.ModelViewSet):
    queryset = Person.objects.all()
    serializer_class = PersonSerializer
    #template_name = "family_tree.html"

class PlaceViewSet(BaseAuthViewSet):
    queryset = Place.objects.all()
    serializer_class = PlaceSerializer

class SourceViewSet(BaseAuthViewSet):
    queryset = Source.objects.all()
    serializer_class = SourceSerializer

class TagViewSet(BaseAuthViewSet):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer

class RelationshipViewSet(LoginRequiredMixin, FamroBaseView, viewsets.ModelViewSet):
    queryset = Relationship.objects.all()
    serializer_class = RelationshipSerializer

class EmigrationViewSet(LoginRequiredMixin, FamroBaseView, viewsets.ModelViewSet):
    queryset = Emigration.objects.all()
    serializer_class = EmigrationSerializer

class DatabaseBackupViewSet(
        LoginRequiredMixin, mixins.CreateModelMixin,mixins.ListModelMixin,
        mixins.RetrieveModelMixin, viewsets.GenericViewSet):

    serializer_class = DatabaseBackupSerializer

    def get_queryset(self):
        pk = self.kwargs.get("pk")
        base_url = 'http://'+get_current_site(self.request).domain
        if pk:
            return DatabaseBackupSerializer.get_backup_list(pk, base_url=base_url)
        return DatabaseBackupSerializer.get_backup_list(base_url=base_url)

    def list(self, request):
        if not request.user.is_superuser and not request.user.is_staff:
            return Response({'status': 'You do not have enough permissions to do that.'})
        return super().list(request)

    def retrieve(self, request, pk=None):
        if not request.user.is_superuser and not request.user.is_staff:
            return Response({'status': 'You do not have enough permissions to do that.'})
        return super().retrieve(request)

    def create(self, request):
        if not request.user.is_superuser:
            return Response({'status': 'You do not have enough permissions to do that.'})
        return super().create(request)

    def update(self, request, pk=None):
        if not request.user.is_superuser:
            return Response({'status': 'You do not have enough permissions to do that.'})
        return super().update(request)

    def partial_update(self, request, pk=None):
        if not request.user.is_superuser:
            return Response({'status': 'You do not have enough permissions to do that.'})
        return super().partial_update(request)

    def destroy(self, request, pk=None):
        if not request.user.is_superuser:
            return Response({'status': 'You do not have enough permissions to do that.'})
        return super().destroy(request)


class SourceQRDownloadView(LoginRequiredMixin, View):
    def get(self, request, source_id=0):
        source_qr_data = ""
        result = ""
        try:
            source_qr_data = Source.objects.get(pk=source_id).qr_image_data
            qr_file = qrcode.make(source_qr_data)
            with io.BytesIO() as output:
                qr_file.get_image().save(output, format="PNG")
                result = output.getvalue()
        except Exception as e:
            print(e)

        if result:
            return HttpResponse(result, content_type='image/png')
        return Response({'status': 'You do not have enough permissions to do that.'})


class SourcePrintableView(LoginRequiredMixin, View):
    def get(self, request, source_id=0, registry_number=0, use_template="", use_spacer="N/A"):
        pdf_data = None
        if not request.user.is_superuser and not request.user.is_staff:
            return Response({'status': 'You do not have enough permissions to do that.'})
        try:
            source = Source.objects.filter(id=source_id).first()
            metadata = source_to_metadata(source, registry_number, use_template=use_template)
            pdf_data = metadata_to_pdf(metadata, use_spacer)
        except Exception as e:
            print(e)
        if pdf_data:
            return HttpResponse(pdf_data, content_type='application/pdf')
        return HttpResponse("", content_type='application/pdf')


class DatabaseBackupDownloadView(LoginRequiredMixin, View):
    def get(self, request, file_name=""):
        if not request.user.is_superuser:
            return Response({'status': 'You do not have enough permissions to do that.'})
        base_path = os.environ.get("FAMRO_DATABASE_BACKUP_FOLDER", "/var/lib/postgresql/data/db_backups")
        return sendfile(request, base_path + "/" + file_name, attachment=True)

    def post(self, request):
        if not request.user.is_superuser:
            return Response({'status': 'You do not have enough permissions to do that.'})
        return super().post(request)


class FamilyTreeView(LoginRequiredMixin, TemplateView):
    template_name = "family_tree.html"

    def get_context_data(self, **kwargs):
        get_person_id_by_uid = lambda x=0: 1
        kwargs.setdefault("person_id", get_person_id_by_uid())
        context = super().get_context_data(**kwargs)
        person = Person.objects.get(pk=kwargs["person_id"])
        context['tree_data'] = person.get_tree_node()
        context['current_person'] = person
        context['menu'] = get_menu(self.request)
        return context
