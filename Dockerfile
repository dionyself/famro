FROM python:3.7.3-alpine3.8
MAINTAINER Dionys Rosario <dionyself@gmail.com>

ENV PGDATA /var/lib/postgresql/data
ENV DJANGO_SETTINGS_MODULE famro.settings
USER root
WORKDIR /srv/famro
COPY . /srv/famro
RUN chown -R postgres /srv/famro/
RUN chmod -R 777 /srv/famro/

RUN apk add --no-cache postgresql=10.10-r0 \
 && apk add --no-cache bash postgresql-dev=10.10-r0 jpeg-dev \
 && apk add --no-cache git \
 && apk add --no-cache build-base zlib-dev \
 && apk add --no-cache linux-headers
# libuuid pcre mailcap gcc libc-dev linux-headers pcre-dev
RUN mkdir -p /run/postgresql/
RUN chmod -R 777 /run/postgresql/

USER postgres
RUN mkdir -p /tmp/shared/uwsgi
RUN mkdir -p /tmp/shared/asgi
RUN chmod -R 777 /tmp/shared/
RUN initdb /var/lib/postgresql/data
RUN mkdir -p /var/lib/postgresql/data/db_backups
RUN echo "host all  all    0.0.0.0/0  md5" >> /var/lib/postgresql/data/pg_hba.conf
RUN echo "listen_addresses='*'" >> /var/lib/postgresql/data/postgresql.conf
USER root
RUN pip3 install -r /srv/famro/requirements.txt \
  && apk del --no-cache build-base zlib-dev
USER postgres

EXPOSE 5432
EXPOSE 8000

# Start app
CMD ["/srv/famro/start.sh"]
