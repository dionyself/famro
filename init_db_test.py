#!/usr/bin/env python
import os
import django
from datetime import date

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "famro.settings")
django.setup()

from django.conf import settings
from famtree.models import make_person, dt, make_match, Person, Relationship, Place, Source, Tag


PLC_altamira = Place.objects.create(country="Republica Dominicana", state="Puerto Plata", city="Altamira")
PLC_santa_cruz_seybo = Place.objects.create(country="Republica Dominicana", state="El Seybo", city="Santa Cruz")
PLC_monte_grande_bahoruco = Place.objects.create(country="Republica Dominicana", state="Bahoruco", city="Monte Grande")

PRS_tpg_jp_dg = make_person(name="Thomas", father_surname="Polanco", mother_surname="Guzman", birth_place=PLC_altamira, baptism_date=date(1811, 1, 21))
juanito_martines = make_person(name="Juanito", father_surname="martinez", birth_place=PLC_monte_grande_bahoruco)
domingo_gonsalez = make_person(name="Domingo", father_surname="gonsalez", birth_place=PLC_monte_grande_bahoruco)

jose_del_rosario = make_person(name="Jose", father_surname="Del Rosario", birth_place=PLC_santa_cruz_seybo)
mariana_de_rodriguez = make_person(name="Mariana", father_surname="rodriguez", birth_place=PLC_santa_cruz_seybo)
m_jmr_vg = make_match(groom=jose_del_rosario, bride=mariana_de_rodriguez)
pedro_del_rosario_martines = make_person(
    match=m_jmr_vg, name="Pedro",
    birth_date=date(1787, 12, 20),
    death_date=date(1891, 12, 9),
    death_place=PLC_santa_cruz_seybo,
    death_witnesses=[juanito_martines, domingo_gonsalez],
    )
