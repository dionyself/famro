"""famro URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from django.conf import settings
from django.conf.urls.static import static
from django.views.decorators.csrf import csrf_exempt
import django.contrib.auth.views
from graphene_django.views import GraphQLView

from rest_framework import routers
from rest_framework import renderers
from famro.views import PrivateGraphQLView
from famtree.views import FamilyTreeView, SourceQRDownloadView, DatabaseBackupDownloadView, \
    DatabaseBackupViewSet, PersonViewSet, PlaceViewSet, SourceViewSet, TagViewSet, \
    RelationshipViewSet, ResidenceViewSet, SourcePrintableView, EmigrationViewSet, \
    LogoutViewSet, CustomAPIRootView

# Routers provide an easy way of automatically determining the URL conf.

router = routers.DefaultRouter()

router.APIRootView = CustomAPIRootView
router.register(r'persons', PersonViewSet)
router.register(r'places', PlaceViewSet)
router.register(r'sources', SourceViewSet)
router.register(r'tags', TagViewSet)
router.register(r'relationships', RelationshipViewSet)
router.register(r'residences', ResidenceViewSet)
router.register(r'emigrations', EmigrationViewSet)
router.register(r'backup', DatabaseBackupViewSet, base_name='backup')
router.register(r'logout', LogoutViewSet, base_name='logout')

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
    #url(r'^graphql', GraphQLView.as_view(graphiql=True)),
    url(r'^graphql', csrf_exempt(PrivateGraphQLView.as_view(graphiql=True))),
    url(r'^accounts/login/$', django.contrib.auth.views.LoginView.as_view()),
    url(r'^backup-download/(?P<file_name>[\w.-]{0,256})$', DatabaseBackupDownloadView.as_view()),
    path(r'source_qr_image/<int:source_id>/', SourceQRDownloadView.as_view()),
    path(r'source_printable_image/<int:source_id>/<int:registry_number>/', SourcePrintableView.as_view()),
    path(r'source_printable_image/<int:source_id>/<int:registry_number>/<use_template>/', SourcePrintableView.as_view()),
    path(r'source_printable_image/<int:source_id>/<int:registry_number>/<use_template>/<use_spacer>/', SourcePrintableView.as_view()),
    url(r'^', include(router.urls)),
    path('tree/', FamilyTreeView.as_view()),
    path('tree/<int:person_id>/', FamilyTreeView.as_view()),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT, show_indexes=True)
