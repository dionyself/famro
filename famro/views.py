from django.contrib.auth.mixins import LoginRequiredMixin
#from graphene_django_subscriptions import depromise_subscription
from graphene_django_extras.views import ExtraGraphQLView
#from .permissions import PermissionsMiddleware


class PrivateGraphQLView(LoginRequiredMixin, ExtraGraphQLView):
    def get_response(self, request, data, show_graphiql=False):
        #kwargs["middleware"].append(PermissionsMiddleware())
        #kwargs["middleware"].append(depromise_subscription)
        if request.user.is_superuser:
            return super().get_response(request, data, show_graphiql=show_graphiql)
        else:
            return "ACCESS DENIED", 404
